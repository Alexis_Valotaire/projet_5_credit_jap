# Projet d'optimisation hydroélectrique
---

Ce projet permet d'optimiser la production de barrage hydroélectrique en fonction de différents facteurs ( débit, volume, etc )  

### Tips git  
Pour merger quand branche behind x commit :  
git fetch origin master  
git pull  

### Prérequis
---

LaTeX  
* Installation:   
  * Windows: Texlive ou Miktex ( nécessite ActivePerl )  
  * Linux: Texlive-full  
  * Éditeur: Visual Studio Code  
  * Extensions: LaTeX Workshop, Rewrap  

Python   
* Version: 3.7.3  
  * IDE: PyCharm  
  * Requirements: [requirements.txt](code/backend/requirements.txt)  

### Installation
---

L'environnemennt de développement Python devrait être installé en suivant les étapes suivantes:

1. Cloner le projet: 

```
git clone ghttps://gitlab.com/Alexis_Valotaire/projet_5_credit_jap.git
```

2. Ce déplacer dans le dossier du projet: 

```
cd projet_5_credit_jap/code/backend/
```

3. Créer un environnement virtuel en utilisant virtualenv (qui peut être installé à l'aide de pip): 

- Windows:  
```
python -m venv venv
```
- Linux/MacOs:  

```
python3 -m venv venv
```

4. Activer la venv: 

- Windows:
```
./venv/Scripts/activate
```

- Linux/MacOS:  
```
source venv/bin/activate
```

5. Installer les requirements: 

```
pip install -r code/backend/requirements.txt
```

6. Lancer l'application:  
```
cd optimizer/
```

```
python app.py
```

Désactivte l'environnement virtuel

```
deactivate
```

## Déploiement
---

Adresse du serveur : hydro-optimizer.ddns.net (138.197.144.139)

## Construit avec
---

* [Flask](https://palletsprojects.com/p/flask/) - Framework web utilisé
* [Bootstrap](https://getbootstrap.com) - Frontend, CSS
* [jQuery](https://jquery.com/) - Framework JS utilisé


## Auteurs
---

* **Alexis Valotaire** - [Alexis_Valotaire](https://gitlab.com/Alexis_Valotaire)
* **Jean-Sébastien St-Pierre** - [jssp](https://gitlab.com/jssp)
* **Pier-Olivier Vermette** - [okhane](https://gitlab/okhane)
