from pulp import *



class probleme():

    def __init__(self, NbCentrale, NbPeriode, Vit_borne, qit_borne, Vinit, apport, D, c, Vfin,C):
        # Create the 'prob' variable to contain the problem data
        prob = LpProblem("optimisation hydroelectricite", LpMaximize)

        # Create problem variables
        Vit_Matrix = []
        for t in range(NbPeriode):
            temp = []
            for i in range(NbCentrale):
                temp.append(LpVariable("Vit" + str(t+1) + str(i+1), Vit_borne[i][0], Vit_borne[i][1]))
            Vit_Matrix.append(temp)


        qit_Matrix = []
        for t in range(NbPeriode):
            temp = []
            for i in range(NbCentrale):
                temp.append(LpVariable("qit" + str(t+1) + str(i+1), qit_borne[i][0], qit_borne[i][1]))
            qit_Matrix.append(temp)

        fonction_Objective = None
        for t in range(NbPeriode):
            for i in range(NbCentrale):
                fonction_Objective += D * (Vit_Matrix[t][i] * C[i][0] + qit_Matrix[t][i] * C[i][1] + C[i][2])


        #  la fonction objective est ajouté au problème
        prob += fonction_Objective

        # contraite de conservation de l'eau
        # centrale1
        prob += Vit_Matrix[1][0] == Vinit[0] - qit_Matrix[0][0] * c + apport[0][0] * c
        for t in range(1, NbPeriode-1):
                prob += Vit_Matrix[t+1][0] == Vit_Matrix[t][0] - qit_Matrix[t][0] * c + apport[t][0] * c
        prob += Vit_Matrix[NbPeriode-1][0] - qit_Matrix[NbPeriode-1][0] * c +\
                apport[NbPeriode-1][0] * c == Vfin[0]

        # centrale2
        prob += Vit_Matrix[1][1] == Vinit[1] - qit_Matrix[0][1] * c + apport[0][1] * c + qit_Matrix[0][0] * c
        for t in range(1, NbPeriode-1):
                prob += Vit_Matrix[t+1][1] == Vit_Matrix[t][1] - qit_Matrix[t][1] * c + apport[t][1] * c + qit_Matrix[t][0] * c
        prob += Vit_Matrix[NbPeriode-1][1] - qit_Matrix[NbPeriode-1][1] * c +\
                apport[NbPeriode-1][1] * c + qit_Matrix[NbPeriode-1][0] * c == Vfin[1]

        # centrale3
        prob += Vit_Matrix[1][2] == Vinit[2] - qit_Matrix[0][2] * c + apport[0][2] * c + qit_Matrix[0][0] * c \
                + qit_Matrix[0][1] * c
        for t in range(1, NbPeriode-1):
                prob += Vit_Matrix[t+1][2] == Vit_Matrix[t][2] - qit_Matrix[t][2] * c + apport[t][2] * c \
                        + qit_Matrix[t][0] * c + qit_Matrix[t][1] * c
        prob += Vit_Matrix[NbPeriode-1][2] - qit_Matrix[NbPeriode-1][2] * c + apport[NbPeriode-1][2] * c\
             + qit_Matrix[NbPeriode-1][0] * c + apport[NbPeriode-1][1] * c == Vfin[2]

        try:
            prob.solve()
            # The status of the solution is printed to the screen
            self.text = "Status:" + str(LpStatus[prob.status]) + "\n"
            # Each of the variables is printed with it's resolved optimum value
            for v in prob.variables():
                self.text += str(v.name) + "=" + str(v.varValue) + "\n"

            # The optimised objective function value is printed to the screen
            self.text += "L'énergie total  = " + str(value(prob.objective)) + "\n"

            fichier_prob = open("resultat.txt", "w")
            fichier_prob.write(self.text)

        except Exception as e:
            print(e)



