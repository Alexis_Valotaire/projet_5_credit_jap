from flask import Flask, request, render_template
from initialisation import *

app = Flask(__name__,
		static_url_path='', 
		static_folder='static',)

@app.route('/')
def appLauncher():
	return render_template('display.html', titre = "Logiciel d'optimisation de la production hydroélectrique")
	# inputData = initialisation("fichierInitialisation.txt", 3, 10, 1)
	# inputData = initialisation("generated_initialisation.txt", 3, 10, 1)
	# result = inputData.extractData()
	# return render_template('display.html', parametres = result.input, resultat = result.solution.text)

#Tests for drag'n drop functionnalities
@app.route('/dragNdrop.html', methods = ['GET', 'POST'])
def getContent():
	if request.method == 'POST':
		htmlVariable = request.form.get('flat')
		return htmlVariable
	else:
		return render_template('dragNdrop.html')

@app.route('/parameters.html', methods = ['GET', 'POST'])
def getUserInput():
	if request.method == 'POST':
		#This should probably be in some kinda loop
		coeffC1 = request.form.get('coeffC1')
		coeffC2 = request.form.get('coeffC2')
		coeffC3 = request.form.get('coeffC3')
		volC1 = request.form.get('volC1')
		volC2 = request.form.get('volC2')
		volC3 = request.form.get('volC3')
		borneVolC1 = request.form.get('borneVolC1')
		borneVolC2 = request.form.get('borneVolC2')
		borneVolC3 = request.form.get('borneVolC3')
		borneDebC1 = request.form.get('borneDebC1')
		borneDebC2 = request.form.get('borneDebC2')
		borneDebC3 = request.form.get('borneDebC3')
		aNbPNbC_C1 = request.form.get('aNbPNbC_C1')
		aNbPNbC_C2 = request.form.get('aNbPNbC_C2')
		aNbPNbC_C3 = request.form.get('aNbPNbC_C3')
		aNbPNbC_C4 = request.form.get('aNbPNbC_C4')
		aNbPNbC_C5 = request.form.get('aNbPNbC_C5')
		aNbPNbC_C6 = request.form.get('aNbPNbC_C6')
		aNbPNbC_C7 = request.form.get('aNbPNbC_C7')
		aNbPNbC_C8 = request.form.get('aNbPNbC_C8')
		aNbPNbC_C9 = request.form.get('aNbPNbC_C9')
		aNbPNbC_C10 = request.form.get('aNbPNbC_C10')
		coeffs = coeffC1 + '\n' + coeffC2 + '\n' + coeffC3
		volumes = volC1 + '\n' + volC2 + '\n' + volC3
		bornesVolume = borneVolC1 + '\n' + borneVolC2 + '\n' + borneVolC3
		bornesDebit = borneDebC1 + '\n' + borneDebC2 + '\n' + borneDebC3
		
		apportsEtc = aNbPNbC_C1 + '\n' + aNbPNbC_C2 + '\n' + aNbPNbC_C3 + '\n' + aNbPNbC_C4 + '\n' + aNbPNbC_C5 + '\n' + aNbPNbC_C6 + '\n' + aNbPNbC_C7 + '\n' + aNbPNbC_C8 + '\n' + aNbPNbC_C9 + '\n' + aNbPNbC_C10
		allOfItTogether = "#coefficient" + '\n' + coeffs + '\n' + "#volume initial et final" + '\n' + volumes + '\n' + "#borne volume" + '\n' + bornesVolume + '\n' + "#borne de debit" + '\n' + bornesDebit + '\n' + "# apport  nombre de periode / nombre de centrale" + '\n' + apportsEtc
		
		mySweetFile = open("generated_initialisation.txt", "w")
		mySweetFile.write(allOfItTogether)
		mySweetFile.close()

		try:
			return solvePbFromFile()

		except:
			return "You fool!!!  Your problem is missing some data!!!"

		#return solvePbFromFile()

		#Was testing something that doesn't work
		# myReadFile = open("generated_initialisation.txt", "r")
		# textFromFile = myReadFile.readlines(27)
		# print(textFromFile)
		# myReadFile.close()
		#return textFromFile
		#return allOfItTogether
	else:
		return app.send_static_file('parameters.html')

def solvePbFromFile():
		#inputData = initialisation("fichierInitialisation.txt", 3, 10, 1)
		inputData = initialisation("generated_initialisation.txt", 3, 10, 1)
		result = inputData.extractData()
		return render_template('display.html', titre2 = "Données d'entrée", titre3 = "Résultats", parametres = result.input, resultat = result.solution.text)

if __name__ == '__main__':
    app.run(debug=True, port=5000)
