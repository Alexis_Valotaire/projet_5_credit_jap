\documentclass[../ingenierie_de_concepts.tex]{subfiles}


\section{Analyse des besoins du client et des exigences des différents intervenants}
\label{sec:analyse_besoin}
Spécification d'exigences logicielles issues de l’analyse des besoins selon la
norme ISO 29110 \cite{ISO29110}.


\subsection{Introduction}
\label{subsec:intro2}


\subsubsection{Objectif}
\label{subsubsec:obj}
L’objectif de la présente section est d’exposer le comportement externe du
«Système d’optimisation de la production hydroélectrique» (SOPH) au niveau
utilisateur. Il expose une liste des exigences fonctionnelles et non
fonctionnelles qui ont été identifiées à partir du cahier des charges ainsi que
lors des rencontres d’évaluation des besoins auxquelles ont pris part les
différents intervenants du projet. Cette section constitue une vue d’ensemble de
la solution logicielle proposée en tenant compte des acteurs et des cas
d'utilisation.
    
\subsubsection{Portée}
\label{subsec:portee}
Le SOPH sera un outil moderne et convivial répondant aux besoins relatifs aux
processus de modélisation et d’optimisation de la production hydroélectrique
court terme. Il devra comporter une interface graphique permettant la
schématisation de réseaux de réservoirs, barrages et centrales hydroélectriques
et la saisie de l’ensemble des paramètres propres à ceux-ci. À la demande de
l’usager, la représentation visuelle obtenue devra être transposée en équations
mathématiques et en séquences codées utilisables par des librairies ou des
solveurs d’usage courant dans le domaine de l’optimisation. Ces derniers seront
invoqués par le SOPH à titre de composants externes. Les résultats seront
retournés au SOPH et utilisés afin de proposer différents types de visualisation
à l’usager (tableaux, graphiques, etc.). La première version du SOPH dont ce
document fait l’objet se restreindra à la résolution de problèmes de type
linéaire.


\iffalse % remove this subsection
\subsubsection{Références}
\label{subsubsec:reference} 
\begin{table}[H]
    \begin{tabular}{ll}
        SOPH-REF-01 & Séguin, S. (2019). Conception d’un logiciel d’optimisation
        \\
         & pour la production hydroélectrique. \\
         & Chicoutimi, Québec : Université du Québec à Chicoutimi. \\[0.4cm]
        SOPH-REF-02 & Bibliothèques de l’Université de Montréal. (2019). \\
         & Citer selon les normes de l’APA. \\
         & Repéré à \url{https://bib.umontreal.ca/bibliographies/apa?tab=108}
         \\[0.4cm]
        SOPH-REF-03 & Laporte, C. (s. d.). Normes ISO/IEC 29110 pour
        l'ingénierie \\
                    & de systèmes et l'ingénierie de logiciels pour les très
                    petits \\ 
                    & organismes. \\
         & Repéré à \url{http://profs.etsmtl.ca/claporte/VSE/Groupe24-menu.html}
         \\[0.4cm]
        SOPH-REF-04 & Ordre des ingénieurs du Québec. (2019). Guide de pratique
        \\ 
                    & professionnelle. \\
         & Repéré à \url{http://www.gpp.oiq.qc.ca/} \\[0.4cm]
        SOPH-REF-05 & Fleming, I. (s. d.). ISO 9126 Software Quality
        Characteristics. \\
         & \url{http://www.sqa.net/iso9126.html } \\ 
    \end{tabular}
\end{table} 
\fi


\subsubsection{Hypothèse et dépendances}
\label{subsubsec:hypothese_dependance}
La validité du présent chapitre repose sur les hypothèses et dépendances
suivantes:
\begin{table}[H]
    \begin{tabularx}{\textwidth}{l X}
        H1 & Les présentes spécifications sont orientées vers la résolution de
        problèmes d’optimisation linéaires. Les parties prenantes sont
        pleinement conscientes de la possibilité que les fonctionnalités du
        système soient étendues ultérieurement. \\[0.3cm]
        H2 & Nous présumons que toute volonté d’apporter des changements aux
        spécifications figurant dans le cahier des charges sera discutée avec
        l’équipe de développement afin d’en évaluer la faisabilité et les
        conséquences sur la production des livrables. \\[0.3cm]
        H3 & Toute demande de changement approuvée entraîneront évidemment la
        mise à jour du présent chapitre et des documents d’ingénierie concernés.
        \\[0.3cm]      
        D1 & Les solveurs requis pour obtenir les solutions aux différents types
        de modèles d’optimisation sont disponibles en version open source. Dans
        le cas contraire, le client dispose des droits et licences nécessaires à
        l’utilisation de ces composants externes. \\[0.3cm]
        D2 & Les infrastructures matérielles qui pourraient s’avérer nécessaires
        (par exemple, un serveur web) sont fournies par le client et il lui
        incombe de les rendre disponibles au moment opportun dans la mesure où
        une demande a préalablement été faite en ce sens dans des délais
        raisonnables.
    \end{tabularx}
\end{table}


\subsection{Les acteurs}
\label{subsec:actors}
\noindent \textbf{SOPH-ACT-001. Professeur-chercheur universitaire} \\
\begin{addmargin}[1cm]{0cm}
    Le professeur-chercheur universitaire est un utilisateur qui se sert du
    système afin de: 
    \begin{itemize}
        \item Modéliser et résoudre des cas d’optimisation hydroélectriques
        \item Visualiser, mettre en contexte et interpréter les résultats
        \item Conseiller les acteurs de l’industrie
        \item Effectuer de temps à autre des tests de validation des résultats
        produits par le système
        \item Diffuser éventuellement le SOPH à la communauté universitaire
        et/ou aux acteurs de l’industrie \\
    \end{itemize}
\end{addmargin} 


\noindent \textbf{SOPH-ACT-002. Producteur hydroélectrique} \\
\begin{addmargin}[1cm]{0cm}
    Le producteur hydroélectrique est un acteur de l’industrie qui se sert du
    système pour:
    \begin{itemize}
        \item Modéliser et résoudre des cas d’optimisation hydroélectriques
        \item Visualiser les résultats
        \item Ajuster les opérations en fonction de ceux-ci \\
    \end{itemize}
\end{addmargin} 


\newpage


\noindent \textbf{SOPH-ACT-003. Administrateur système} \\
\begin{addmargin}[1cm]{0cm}
    L’administrateur système est un acteur dont la responsabilité principale est
    d’assurer le bon fonctionnement et la pérennité du système, notamment par:
    \begin{itemize}
        \item La gestion des comptes utilisateurs, des permissions et de la
        sécurité
        \item Le monitorage du fonctionnement et des performances du serveur
        \item La résolution de problèmes de compatibilité potentiels inhérents à
        l’évolution des navigateurs web
        \item La maintenance de l’application \\
    \end{itemize}
\end{addmargin} 


\noindent \textbf{SOPH-ACT-004 Solveur d'optimisation} \\
\begin{addmargin}[1cm]{0cm}
    Le solveur d’optimisation est un composant logiciel externe qui est appelé
    par le système pour: 
    \begin{itemize}
        \item Recevoir en entrée les paramètres appropriés en fonction du type
        de modèle d'optimisation à résoudre
        \item Effectuer la résolution du modèle d’optimisation
        \item Retourner les résultats au système \\
    \end{itemize}
\end{addmargin} 


\subsection{Survol du Modèle des Cas d’Utilisation}
\label{subsec:survol_cas}
Le survol du modèle des cas d’utilisation illustre les principales
fonctionnalités du SOPH en fonction des besoins des acteurs identifiés ci-haut.
Par la suite, les cas d’utilisation seront énoncés selon le format bref. Le
lecteur pourra consulter au besoin la description détaillée selon le format
étendu à l’annexe \ref{sec:annexe1}.


\begin{figure}[H]
    \centerline{\includegraphics[width=18cm, height=38cm, keepaspectratio]{Diagramme_cas_utilisation}}
    \caption{Diagramme du modèle des cas d’utilisation}
    \label{fig:modele_cas_utilisation} 
\end{figure}


\subsubsection{Présentation des cas d’utilisation}
\label{subsubsec:present_use_case}
Afin de simplifier la lecture, les cas d’utilisation sont présentés ci-dessous
selon le format bref du processus unifié. \\[0.3cm]

\noindent \textbf{SOPH-UCS-001. S’authentifier}
\begin{addmargin}[1cm]{0cm}
    L’utilisateur saisit un nom d’utilisateur et un mot de passe permettant
    d’accéder à la fenêtre principale du système et aux fonctionnalités
    autorisées selon le niveau d’accréditation. \\[0.3cm]
Acteurs concernés: Professeur-chercheur / Producteur hydroélectrique \\
\end{addmargin} 


\noindent \textbf{SOPH-UCS-002. Charger les paramètres du modèle à partir d’un fichier texte}
\begin{addmargin}[1cm]{0cm}
    L’utilisateur clique sur un bouton permettant de charger un fichier
    contenant toutes les informations permettant au système de générer le modèle
    d’optimisation ainsi que les icônes requises pour la schématisation.
    \\[0.3cm]
    Acteurs concernés: Professeur-chercheur / Producteur hydroélectrique \\
\end{addmargin} 


\newpage


\noindent \textbf{SOPH-UCS-003. Schématiser un système de production hydroélectrique à l’aide des icônes proposées par le système}
\begin{addmargin}[1cm]{0cm}
    Dans la fenêtre principale, l’utilisateur clique sur des icônes représentant
    des centrales hydroélectriques qu’il peut glisser et déposer dans la zone de
    canevas de conception selon l’agencement souhaité. Les icônes disponibles
    dépendent des paramètres préalablement chargés. L’utilisateur peut ensuite
    les relier à l’aide de connecteurs prévus à cet effet. Alternativement,
    l’utilisateur peut également charger un fichier de schématisation existant.
    \\[0.3cm]
    Acteurs concernés: Professeur-chercheur / Producteur hydroélectrique \\
\end{addmargin} 
   
% \noindent \textbf{SOPH-UCS-004. Générer le code du modèle d’optimisation à partir des intrants}
% \begin{addmargin}[1cm]{0cm}
%     Une fois la schématisation et la paramétrisation complétées, l’utilisateur
%     clique sur un bouton entraînant la génération automatique du code du modèle
%     d’optimisation dans le format adéquat selon le type de problème et le
%     solveur requis. \\[0.3cm]
%     Acteurs concernés: Professeur-chercheur / Producteur hydroélectrique / Solveur d'optimisation \\

\noindent \textbf{SOPH-UCS-004. Lancer la résolution du modèle d’optimisation}
\begin{addmargin}[1cm]{0cm}
    Le système génère le code approprié selon le type de modèle et invoque solveur approprié en lui transmettant le code. Le système récupère ensuite les résultats.
    \\[0.3cm]
    Acteurs concernés: Solveur d’optimisation \\
\end{addmargin} 
\noindent \textbf{SOPH-UCS-005. Visualiser les résultats}
\begin{addmargin}[1cm]{0cm}
    L’utilisateur sélectionne un des outils de visualisation des résultats
    retournés par le solveur. Il peut s’agir, par exemple, de tableaux,
    graphiques, fichiers de sortie, etc. \\[0.3cm]
    Acteurs concernés: Professeur-chercheur / Producteur hydroélectrique \\
\end{addmargin} 
\noindent \textbf{SOPH-UCS-006. Sauvegarder et/ou imprimer}
\begin{addmargin}[1cm]{0cm}
    L’utilisateur clique sur le bouton approprié selon la fonctionnalité
    désirée. La sauvegarde peut concerner le schéma, le modèle mathématique, les
    résultats ou toutes ces options. \\[0.3cm]
    Acteurs concernés: Professeur-chercheur / Producteur hydroélectrique \\
\end{addmargin} 
\noindent \textbf{SOPH-UCS-007. Administrer le système}
\begin{addmargin}[1cm]{0cm}
    L’administrateur système gère les comptes utilisateurs, veille au bon
    fonctionnement du système et intervient au besoin. Il effectue les
    maintenances et mises à jour requises sur le serveur et s’assure de gérer
    toute problématique potentielle. \\[0.3cm]
Acteurs concernés: Administrateur système 
\end{addmargin} 
     
\subsection{Les exigences}
\label{subsec:les_exigences}

\subsubsection{Les exigences fonctionnelles}
\label{subsec:les_exigences_fonctionnelles}


\noindent \textbf{SOPH-FSR-001. Le logiciel doit permettre l'authentification et la déconnexion des utilisateurs (côté serveur) au moyen d'interfaces dédiées (côté client) permettant de:}
\begin{itemize}
    \item Saisir et valider les identifiants et mots de passe afin d’accorder
    ou de bloquer l’accès au système
    \item Rediriger l’usager vers la fenêtre principale du système
    \item Fermer une session de travail de manière sécuritaire
    \item Assurer un niveau de sécurité adéquat en préservant la confidentialité et l'intégrité des noms d'usager/motes de passe au moyen des standards cryptographiques reconnus.  \\[-0.4cm]
\end{itemize}
\begin{addmargin}[1cm]{0cm}         
    Acteurs concernés: Professeur-chercheur / Producteur hydroélectrique /
    Administrateur système \\
\end{addmargin}

\noindent \textbf{SOPH-FSR-002. Le logiciel doit permettre à l’utilisateur de
charger les paramètres du modèle à partir d’un fichier en format .txt} \\[0.3cm]
Le fichier doit contenir les coefficients de la fonction objectif, les bornes et
les apports naturels. Plus spécifiquement, on y retrouve :
\begin{itemize}
    \item Le nombre de périodes 
    \item La durée des périodes
    \item Les débits minimums et maximums pour chaque centrale en fonction des
    périodes
    \item Les volumes minimums et maximums pour chaque centrale en fonction des
    périodes
    \item Les apports naturels pour chaque centrale en fonction des périodes. On
    néglige pour le moment l'incertitude associée aux apports naturels, ce
    qui permet d'obtenir un modèle d'optimisation déterministe. \\[-0.4cm]
\end{itemize}
\begin{addmargin}[1cm]{0cm}         
    Acteurs concernés: Professeur-chercheur / Producteur hydroélectrique \\
\end{addmargin}

\noindent \textbf{SOPH-FSR-003. Le logiciel doit permettre à l'usager de sauvegarder des fichiers sur le serveur.  Plus particulièrement, le système doit comporter:}
\begin{itemize}
    \item Un bouton permettant la sauvegarde du fichier d'entrée et/ou du schéma du réseau de production hydroélectrique
    \item Un bouton permettant la sauvegarde du fichier de résultats
    \item Un ou des bouton(s) permettant la sauvegarde des représentations graphiques des résultats. \\[-0.4cm]
\end{itemize}
\begin{addmargin}[1cm]{0cm}         
    Acteurs concernés: Professeur-chercheur / Producteur hydroélectrique \\
\end{addmargin}

\noindent \textbf{SOPH-FSR-004. Le logiciel doit permettre l'impression du fichier/schéma d'entrée, du fichier de résultats et des représentations graphiques de ces derniers.  On doit donc retrouver:}
\begin{itemize}
    \item Un bouton permettant l'impression du fichier d'entrée et/ou du schéma du réseau de production hydroélectrique
    \item Un bouton permettant l'impression du fichier de résultats
    \item Un ou des bouton(s) permettant l'impression des représentations graphiques des résultats. \\[-0.4cm]
\end{itemize}
\begin{addmargin}[1cm]{0cm}         
    Acteurs concernés: Professeur-chercheur / Producteur hydroélectrique \\
\end{addmargin}

\noindent \textbf{SOPH-FSR-005. Le logiciel doit être en mesure d'extraire les données d'entrée à partir du fichier/schéma fourni.  À cet effet, le système doit:}
\begin{itemize}
    \item Effectuer la validation du fichier/schéma par rapport au standard déterminé
    \item En extraire les paramètres du modèle mathématique, puis les conserver en mémoire.
    \item Lorsque demandé par l'utilisateur, appeler le solver requis et lui transmettre les paramètres du modèle.\\[-0.4cm]
\end{itemize}
\begin{addmargin}[1cm]{0cm}         
    Acteurs concernés: Solveur d'optimisation / Professeur-chercheur / Producteur hydroélectrique \\
\end{addmargin}


\noindent \textbf{SOPH-FSR-006. Le logiciel doit permettre la résolution des problèmes d’optimisation dont les fonctions de production sont linéaires et de type :}
\begin{itemize}
    \item Polynomial total, où la solution constitue le débit total et le
    volume, pour chaque centrale et pour chaque période \\[-0.4cm]
\end{itemize}
\begin{addmargin}[1cm]{0cm}         
    Acteurs concernés: Professeur-chercheur / Producteur hydroélectrique /
    Solveur d’optimisation \\
\end{addmargin} 

\noindent \textbf{SOPH-FSR-007. Le système doit comporter des fonctionnalités permettant la visualisation et l'interprétation des résultats.  L'usager doit pouvoir accéder à:}
\begin{itemize}
    \item La représentation textuelle des résultats, générés par le solveur d'optimisation sous forme de fichier texte
    \item Un ou plusieurs mode(s) de représentation graphique des données contenues dans le fichier de résultats.\\[-0.4cm]
\end{itemize}
\begin{addmargin}[1cm]{0cm}         
    Acteurs concernés: Professeur-chercheur / Producteur hydroélectrique /
    Solveur d’optimisation \\
\end{addmargin} 


\noindent \textbf{SOPH-FSR-008. Le logiciel doit comporter une interface graphique principale s’affichant dans un navigateur web et permettant :}
\begin{itemize}
    \item L’accès direct, au moyen d’un bouton, à la fonctionnalité
    d’importation de fichiers de paramètres
    \item L’accès direct aux fonctionnalités de sauvegarde/impression de
    fichiers
    \item L’accès direct aux icônes et symboles représentant les éléments de
    schéma de réseau de production hydroélectrique
    \item La schématisation du réseau de production dans une zone canevas située
    au centre de l’écran.
    \item La saisie des paramètres du modèle dans un formulaire dédié
    \item L’accès direct à un bouton “Run” permettant de lancer la résolution du
    modèle
    \item L’accès à l’ensemble des sous-fonctionnalités du système ne figurant
    pas dans l’interface principale. \\[-0.4cm]
\end{itemize}
\begin{addmargin}[1cm]{0cm}         
    Acteurs concernés: Professeur-chercheur / Producteur hydroélectrique \\
\end{addmargin}


\noindent \textbf{SOPH-FSR-009. Le logiciel doit comporter des interfaces graphiques secondaires permettant notamment :}
\begin{itemize}
    \item La visualisation des résultats sous la forme demandée
    et la sauvegarde/impression de ceux-ci
    \item L’accès en lecture et écriture aux fichiers de paramètres/schémas
    sauvegardés sur le serveur dans le dossier associé au compte de
    l’utilisateur
    \item La gestion du profil de l’utilisateur (modification des informations
    personnelles, du mot de passe, etc) \\[-0.4cm]
\end{itemize}
\begin{addmargin}[1cm]{0cm}         
    Acteurs concernés: Professeur-chercheur / Producteur hydroélectrique
\end{addmargin}


\subsubsection{Les exigences non fonctionnelles}
\label{subsec:les_exigences_non_fonctionnelles}
Les exigences non fonctionnelles suivantes sont basées sur les standards énoncés
par la norme ISO/IEC 9126 \cite{iso9126}. À ce stade préliminaire, nous
présentons une vue de haut niveau des éléments les plus pertinents. \\
    
\noindent \textbf{SOPH-NFR-001. Fonctionnalité}
\begin{itemize}
    \item Exactitude: \\
Le système produit des résultats mathématiquement exacts en fonction des entrées
fournies.


            \item Sécurité: \\
Le système accorde les droits et accès appropriés en fonction des privilèges de
chaque utilisateur. Les standards de sécurité en matière de communication réseau
sont respectés. \\
\end{itemize}


\noindent \textbf{SOPH-NFR-002. Fiabilité}
\begin{itemize}
    \item Tolérance à l’erreur: \\
Le système valide les entrées et invite l’utilisateur à effectuer les correctifs
appropriés en cas de non-conformité.


    \item Recouvrabilité: \\
Le système est conçu de manière à gérer les entrées invalides, les événements
inattendus et les exceptions afin d’être en mesure de récupérer de celles-ci et
de retourner à son fonctionnement normal. \\
\end{itemize}


\noindent \textbf{SOPH-NFR-003. Convivialité}
\begin{itemize}
    \item Facilité d’utilisation: \\
Le fonctionnement du système est intuitif et l’apprentissage de son utilisation
requiert un temps minimal (< 1 heure)


    \item Opérabilité: \\
Le système est conçu de manière à demeurer dans un état stable et opérable en
tout temps


    \item Attrayabilité: \\
Le design des interfaces utilisateur est esthétique et attrayant. \\
\end{itemize}


\noindent \textbf{SOPH-NFR-004. Efficience}
\begin{itemize}
    \item Utilisation des ressources: \\
Le design du système vise une utilisation optimale des ressources matérielles et
logicielles en tout temps


    \item Temps de réponse: \\
Le système est conçu de manière à minimiser le temps requis pour compléter
chacune des opérations demandées. \\
\end{itemize}


\noindent \textbf{SOPH-NFR-005. Maintenabilité}
\begin{itemize}
    \item Analysabilité: \\
L’architecture, le design, le code et les commentaires sont conçus de manière à
être facilement analysables et compréhensibles pour tout programmeur. Le
développement modulaire est employé afin de délimiter clairement la portée de
chaque fonctionnalité. 


    \item Testabilité: \\
Le développement modulaire permet les tests unitaires ainsi que les tests
système. Les tests automatisés sont programmés pour chaque fonctionnalité au fur
et à mesure de leur développement. \\
\end{itemize}


\noindent \textbf{SOPH-NFR-006. Portabilité}
\begin{itemize}
    \item Installabilité: \\
Puisqu’il s’agit d’une application web, la portabilité est étendue à tout
système doté d’un navigateur internet moderne et ne requiert aucune forme
d’installation. \\
\end{itemize}
        
\subsection{Documentation en direct pour l’utilisateur et exigences du système d’aide}
\label{subsec:documentation}
\noindent \textbf{SOPH-DSR-001 : Langage des documents}
\begin{addmargin}[1cm]{0cm}         
    Tous les documents doivent être rédigés en français.
\end{addmargin}
    
\subsection{Contraintes de conception}
\label{subsec:contraintes_de_conception}
\noindent \textbf{SOPH-CC-001 : Forme du logiciel}
\begin{addmargin}[1cm]{0cm}         
        Afin de faciliter la diffusion du logiciel, il est de mise de le
        concevoir sous la forme d’une application web. \\      \end{addmargin}
        
\noindent \textbf{SOPH-CC-002 : Complexité et développement itératif}
\begin{addmargin}[1cm]{0cm}         
        La complexité des différents modèles d’optimisation utilisés par
        l’industrie impose une démarche de développement itérative de type Agile
        \cite{agile}. Les cas les plus simples sont d’abord implémentés (modèles
        linéaires tels que présentés ci-haut). Par la suite, la modélisation et
        la résolution d’autres types de modèles plus complexes pourront être
        implémentées si l’échéancier le permet. \\
\end{addmargin}
        
\subsection{Composants achetés}
\label{subsec:comp_achete}
    \textbf{N/A}
    
\subsection{Interfaces}
\label{subsec:interfaces}


\subsubsection{Interfaces utilisateur}
\label{subsubsec:Int_utilisateur}
Des interfaces graphiques web primaires et secondaires permettent à
l’utilisateur d’accéder à l’ensemble des fonctionnalités du système. Le lecteur
peut en consulter les premières ébauches aux figures 6 et 7.
    
\subsubsection{Interfaces matérielles}
\label{subsubsec:Int_materielles}
\textbf{N/A}
    
\subsubsection{Interfaces logicielles}
\label{subsubsec:Int_logicielles}

\noindent \textbf{SOPH-IFL-001}
\begin{addmargin}[1cm]{0cm}         
    Interface logicielle entre le \textit{front-end} et le \textit{back-end} implémentée conformément au standard HTTP/HTTPS. \\     
\end{addmargin}

\noindent \textbf{SOPH-IFL-002}
\begin{addmargin}[1cm]{0cm}         
    Interface logicielle entre le serveur web et le SOPH, dont la nature demeure à déterminer en fonction des choix qui seront effectués concernant le type de serveur, les langages de programmation et les \textit{frameworks} de développement. \\        
\end{addmargin}

\noindent \textbf{SOPH-IFL-003}
\begin{addmargin}[1cm]{0cm}         
    Interfaces entre le SOPH et les différents solveurs et librairies qui seront
    employés pour résoudre les modèles mathématiques.                \\
\end{addmargin}

\noindent \textbf{SOPH-IFL-004}
\begin{addmargin}[1cm]{0cm}         
    Interfaces entre le SOPH et le système d'exploitation du client (fonctionnalités de sauvegarde et d'impression).  Cet interfaçage est habituellement assuré nativement par des fonctions intégrées aux langages de programmation de haut niveau et ne devrait pas demander de développement particulier. \\
\end{addmargin}
    
\subsubsection{Interfaces de communications}
\label{subsubsec:Int_communication}
\noindent \textbf{SOPH-IFC-001}
\begin{addmargin}[1cm]{0cm}         
    Un port TCP dédié sera utilisé par les systèmes clients qui accèderont au
    serveur distant via internet. \\                         
\end{addmargin}


\noindent \textbf{SOPH-IFC-002}
\begin{addmargin}[1cm]{0cm}         
    Une plage de ports TCP dédiés sera utilisée par le serveur pour accepter les
    connexions des ordinateurs clients.          
\end{addmargin}
        
\subsection{Exigences de licences}
\label{subsec:exigence_licences}
Il n'y a aucune exigence de licence puisque toute technologie utilisée sera
libre de droits.
    
\subsection{Remarques légales, droits d'auteur et diverses}
\label{subsec:remarques_legales}
L’ensemble des documents, logiciels et autres livrables produits dans le cadre
de ce projet demeurent la propriété exclusive de leurs auteurs.
    
\subsection{Standards applicables}
\label{subsec:standards}
Le processus de développement ainsi que le produit résultant doivent respecter
les modèles de qualité ISO 29110 et ISO/IEC 9126 ainsi que le standard de
sécurité ISO 27002.