\documentclass[../ingenierie_de_concepts.tex]{subfiles}


\section{Revue de l'état de l'art}
\label{sec:etat_art}
Le projet de conception du SOPH se découpe en plusieurs sous-systèmes qui
doivent interagir entre eux de manière simple et efficace. Ces sous-systèmes
incluent:


\begin{itemize}
    \item Un ou plusieurs composant(s) logiciel(s) de service web HTTP/HTTPS
    (côté serveur)
    \item Un ou plusieurs composant(s) logiciel(s) gérant les interactions
    dynamiques avec l'usager (côté serveur/côté client)
    \item Un composant logiciel transposant les intrants en modèle mathématique
    utilisable par le solveur approprié (côté serveur)
    \item Un ou plusieurs composant(s) logiciel(s) calculant et retournant la
    solution optimisée du modèle fourni (côté serveur)
    \item Un ensemble de composants logiciels permettant la réalisation des
    interfaces utilisateur (côté client)
    \item Un ensemble de composants logiciels effectuant la mise en forme des
    interfaces utilisateur (côté client) \\
\end{itemize}



Cette section a pour but d'effectuer une revue comparée des options
technologiques dont l'usage est susceptible de présenter un intérêt pour chacun
des sous-systèmes identifiés ci-haut.


\subsection{Composants logiciels de service web HTTP/HTTPS}
\label{subsec:web_server}


Les logiciels de service web, parfois appelés serveurs web (terme qui porte à
confusion avec la machine physique sur lesquels ils sont déployés), ont pour
principale utilité de gérer les requêtes de clients (plus communément les
navigateurs web) qui leur sont transmises au moyen du protocole HTTP (Hypertext
Transfer Protocol) ou de sa version sécurisée HTTPS. Le protocole HTTP concerne
la couche applicative du modèle OSI \cite{http}. Le fonctionnement de base des
logiciels de service web est simple: ils sont conçus pour "écouter" sur un port
de communication dédié et réagir lorsqu'ils sont invoqués via leur URL, combinée
à une ou plusieurs commande(s) décrite(s) dans la spécification du protocole
HTTP \cite{http_spec}. La réaction en question implique habituellement le
traitement des données envoyées par le client, s'il y a lieu, suivi du renvoi
par le serveur des informations demandées. Il peut s'agir simplement de données
destinées à être affichées par le client au moyen d'une page web HTML. Le
serveur renvoie alors un fichier HTML qui sera interprété par le navigateur du
client. L'information demandée peut également concerner un fichier d'un autre
type (image, texte, etc.), qui sera renvoyé au client et ouvert au moyen du
logiciel approprié. Les méthodes les plus courantes décrites par la
spécification du HTTP sont le 'GET' (requête pour obtenir une ressource du
serveur) et le 'POST' (requête impliquant l'envoi d'une ressource du client vers
le serveur). \\


Le logiciel de service web le plus répandu est Apache (version 2.x), un produit
multi-plate-forme open source lancé en 1995 et en développement constant sous
l'égide de The Apache Software Foundation \cite{apache_server}. La dernière
version en date d'aujourd'hui (v. 2.4.41) a été lancée en août 2019. Selon
w3Techs, la technologie Apache accapare actuellement 44\% du marché
\cite{w3t_webservers}, loin devant son plus proche rival NGINX. Le serveur Apache
inclut nativement le support des protocoles SMTP et FTP, de l'encryption
SSL/TLS, comporte des modes proxy/proxy inversé et est compatible avec tous les
systèmes d'exploitation courants \cite{apache_features}. Sa robustesse n'est
plus à démontrer et sa gratuité en fait un choix de premier intérêt pour la
plupart des applications courantes. \\


Le second logiciel de service web le plus populaire est NGINX
\cite{w3t_webservers}, dont la première version a été lancée en 2004
\cite{nginx}. Comme Apache, il s'agit également d'un produit open source. En
plus des protocoles HTTP/HTTPS, NGINX supporte le mode de proxy inversé ainsi
que le mode proxy pour les protocoles TCP/IP et UDP. L'architecture asynchrone
et non bloquante de NGINX est conçue de manière à permettre la gestion d'un très
grand nombre de connexions simultanées, ce qui pouvait historiquement présenter
un problème avec la technologie Apache \cite{why_nginx}. Bien que
multi-plate-forme, NGINX ne performe pas aussi efficacement dans l'environnement
Windows, contrairement à Apache. NGINX est aussi disponible gratuitement. \\


En queue de peloton des logiciels de service web les plus répandus, on retrouve
Microsoft IIS \cite{w3t_webservers}. Contrairement à Apache et NGINX, il s'agit
d'un logiciel propriétaire (et donc payant) conçu spécialement pour les serveurs
utilisant l'architecture Windows NT \cite{w3t_iis}. Son utilisation est
répandue dans les environnements informatiques basés sur les plateformes de
Microsoft. Sa dernière version (v. 10) date de 2015.



\subsection{Composants logiciels gérant les interactions dynamiques avec l'usager}
\label{subsec:dynamic_content}


Les composants logiciels permettant l'interaction dynamique entre l'usager et le
serveur web permettent à ce dernier de répondre à des requêtes dynamiques. Par
opposition aux requêtes statiques, où l'appel à une URL entraîne un comportement
invariable de la part du serveur, les requêtes dynamiques incluent la
transmission d'informations qui seront traitées par celui-ci dans le but de
produire une réponse adaptée au contexte \cite{dynamic_web}. Il s'agit donc de
composants applicatifs qui sont jumelés à un logiciel de service web. On réfère
communément à cet ensemble serveur web - composants logiciels dynamiques au
moyen du terme générique \textit{back-end}, désignant les processus de traitement de
l'information du côté serveur et qui sont transparents pour l'utilisateur.\\


Plusieurs langages de programmation permettent d'implémenter la logique de ces
interactions. Un des plus connus est le JavaScript, qui est un langage
interprété, léger, portable et extrêmement répandu \cite{Javascript}. Bien
qu'on le retrouve plus souvent en \textit{front-end} (les navigateurs modernes disposent
d'un interpréteur JavaScript intégré), il est fréquemment employé dans d'autres
types d'applications, notamment en \textit{back-end}. La plateforme Node.js en est un
exemple \cite{nodejs}. \\


Le PHP est un autre langage classique pour l'implémentation de la logique
\textit{back-end}. Il s'agit d'un langage impératif orienté-objet faisant lui aussi
appel à un interpréteur. Créé en 1994, il demeure encore à ce jour le plus
utilisé côté serveur pour la logique et les interactions dynamiques
\cite{php_stats}. \\


Ensuite, mentionnons la contribution de Microsoft à l'univers du développement
web avec son \textit{framework} ASP.NET, basé sur la plateforme .NET \cite{asp.net}. Cet
environnement permet l'intégration du C\# en tant que langage \textit{back-end} ainsi que
l'utilisation des nombreuses librairies et fonctionnalités du noyau .NET, en
plus d'assurer une compatibilité avec les autres outils de Microsoft (SQL
Server, par exemple). Le cadre ASP.NET se situe en quelque sorte à l'opposé de
ses compétiteurs en termes de légèreté et de simplicité. \\


Finalement, il y a également le langage Python \cite{python_home}, qui gagne
sans cesse en popularité dans tous les domaines de la programmation. Python est
un langage de haut niveau, interprété, orienté-objet, placé sous licence libre
et dynamiquement typé. Il a été créé au début des années 90 par Guido van Rossum
\cite{pythonLang}. Déclaré "Langage de l'année 2018" par l'indice Tiobe
\cite{pythonpop}, Python permet un apprentissage rapide pour quiconque maîtrise
les bases de la programmation. Il permet de réaliser des implémentations
robustes en très peu de temps, comparativement aux langages de niveau
inférieur. Ses performances sont toutefois limitées par une absence de support
du multi-threading. Cette lacune est cependant compensée grâce à l'utilisation
de librairies écrites et compilées en C/C++, appelées depuis le code Python. Il
existe d'ailleurs une quantité impressionnante de librairies destinées à des
usages variés, allant du traitement d'images sophistiqué au \textit{machine
learning/deep learning} en passant par l'optimisation. \\



Il existe également des composants logiciels dynamiques destinés à être utilisés
du côté client. Le plus courant est le JavaScript, présenté plus haut, dont
l'interpréteur est intégré dans tous les navigateurs web modernes. Il est
utilisé pour les interactions locales ne nécessitant pas qu'une requête soit
effectuée auprès d'un serveur web \cite{javascript_full}. Par exemple, les
fonctionnalités glisser-déposer présentes sur une page ou une application web
sont gérées du côté client, le plus souvent au moyen de code JavaScript. Dans
le même sens, le JavaScript est couramment employé pour modifier dynamiquement
des éléments de contenu d'une page HTML à partir d'entrées fournies par
l'usager. \\



\subsection{Composant logiciel transposant les intrants en modèle mathématique d'optimisation}
\label{subsec:translate_to_model}


D'après les spécifications logicielles présentées à la section
\ref{sec:analyse_besoin}, il existe deux mécanismes par lesquels l'usager
fournira les intrants nécessaires à la formulation du modèle mathématique
d'optimisation:


\begin{itemize}
    \item L'usager téléverse un fichier texte contenant toutes les données
    nécessaires à la formulation du modèle
    \item L'usager schématise son réseau de production hydroélectrique et saisit
    manuellement les paramètres additionnels requis
\end{itemize}



Le premier cas nécessite l'extraction des données à partir d'un fichier texte
tandis que le second requiert une extraction équivalente à partir d'une
évaluation du schéma et des valeurs entrées dans les cases prévues à cette fin.
Comme ces tâches peuvent être accomplies par n'importe quel langage de
programmation, l'enjeu à analyser concerne surtout la méthode de transmission
des intrants au composant chargé de l'extraction. Les bonnes pratiques en
matière de sécurité informatique commandent la recherche de la simplicité ainsi
que la minimisation des mécanismes d'accès aux ressources
\cite{principes_securite}. Il convient dès lors d'envisager un seul mode de
transmission des entrées de l'usager, probablement sous la forme d'un fichier
texte envoyé du client au serveur. Quant au langage de programmation à utiliser
pour le composant d'extraction des données, il serait judicieux d'employer le
même qui sera choisi pour le \textit{back-end} du serveur web. Ainsi, ces deux
composants pourront s'échanger des données dans la plus parfaite harmonie. \\


\subsection{Composants logiciels calculant et retournant la solution du modèle d'optimisation}
\label{subsec:model_solvers}


Afin d'effectuer la résolution du modèle d'optimisation, il est nécessaire de
recourir à des outils informatiques spécialisés en la matière. Ces outils sont
disponibles sous forme de logiciels propriétaires (ex. AMPL, MatLab) et sous
forme de librairies ou API associées à un ou plusieurs langage(s) de
programmation disposant eux-mêmes de fonctionnalités plus ou moins attrayantes
selon l'objectif poursuivi. \\


Pour le composant chargé d'effectuer l'optimisation proprement dite, quatre
langages de programmation se démarquent du lot, à savoir le Python, le R, le
Julia ainsi que le MATLAB. Les lignes qui suivent visent à décrire chacun de ces
langages tout en listant les avantages et désavantages liés à leur utilisation
dans le contexte de l’optimisation mathématique. \\


Le Python a déjà été introduit plus haut dans cette section. L’utilisation de
librairies d’optimisations telles que SciPy, Numpy et Pulp est nécessaire afin
d’être en mesure d’accomplir les tâches d’optimisations. L'avantage principal
du Python dans ce contexte concerne justement la quantité appréciable de
librairies d'optimisation bien documentées, libres et disponibles gratuitement. Cependant, du
côté des désavantages, le fait qu'il s'agisse d'un langage interprété entraîne
une lenteur d'exécution non négligeable comparativement aux langages du plus
bas niveau tel le C. Cet inconvénient est mitigé par des librairies comme
Numba, qui optimise le bytecode Python en fonction des capacités du CPU et
utilise ensuite la compilation à la volée (\textit{just-in-time compiling}, ou JIT)
pour le traduire en code machine LLVM \cite{numba_jit}. \\

Le R possède les mêmes caractéristiques fondamentales que le Python, soit d’être
un langage interprété, orienté objet, libre de droits et dynamiquement typé.
Par contre, il est spécialement destiné à la science des données, contrairement
au Python, d'usage plus général. Le R a été développé dans les laboratoires de
Bell par John Chambers et ses collègues \cite{rLang}. Il possède comme
avantages la disponibilité d'une abondante documentation en raison de la grande
communauté d’utilisateurs, la possibilité d’intégrer d’autres langages tel le
C/C++, Java et Python et la facilité d'implémentation d'applications
parallèles. Ses inconvénients sont une courbe d’apprentissage abrupte, un temps
d’exécution lent comparativement aux autres langages évalués dans cette section
et la rigidité du standard de programmation, qui doit être respecté à la lettre
sans quoi le code devient rapidement impropre. \\
    
Le MATLAB est aussi un langage interprété, multi paradigmes, dynamiquement typé,
mais il n'est pas libre de droits. Il est nécessaire d'acheter une licence
afin de pouvoir l'utiliser. Il a été créé vers la fin des années 70 par Cleve
Moler \cite{matlabLang}. Ce langage possède comme avantages de disposer d'une
importante quantité de fonctions mathématiques pré-programmées et possède une
communauté d’utilisateurs scientifiques importante. Par contre, les coûts associés à une licence 
se chiffrent en milliers de dollars \cite{matlablicense}, plusieurs
fonctions sont propriétaires (le code source ne peut être consulté/édité), 
il est plus lent que le Python (avec Numba) et que le Julia et son manque de polyvalence 
fait en sorte qu'il est difficile d'envisager la création d'une application web basée sur ce langage. MATLAB
propose toutefois une API pour Python permettant d'exécuter du code MATLAB
embarqué dans une application Python \cite{matlab_api}. \\
    
Le Julia est un langage compilé, contrairement à Python, R et MATLAB, ce qui le
rend beaucoup plus rapide que ces derniers. Le Julia est aussi un langage de
haut niveau, libre de droits, dynamiquement typé et destiné aux calculs
scientifiques. Il a été créé en 2012 par Jeff Bezanson, Stefan Karpinski, Viral B.
Shah et Alan Edelman. Cependant, la version 1.0 n'a vu le jour que le 8 août
2018 \cite{juliaLang}. Les avantages de son utilisation sont sa rapidité
d'exécution, la disponibilité de librairies d'optimisation ainsi que la
possibilité de faire des appels au C, Fortran et Python. Cependant, le Julia
nécessite un temps de précompilation assez long, ce qui peut être ennuyeux
lorsque le temps requis pour la compilation du code est supérieur au temps
d'exécution. Son outil de débogage est réputé moins efficace que celui du
Python ou du R, ses \textit{framework}s webs ne sont pas encore tout à fait au point et
sa relative nouveauté implique une documentation limitée.



% \begin{table}[H] \centering \selectlanguage{french} 
% \caption{Temps de calcul de plusieurs langages pour une même fonction
% \cite{comp_lang}} \vspace{0.3cm}
% \includegraphics[width=8cm]{benchmark_langages} \label{tab:bench_table}
% \end{table} 


% Le tableau \ref{tab:bench_table} permet d'observer que parmi les quatre
% langages amenés plus haut, le Matlab est le plus rapide, suivi par le Python
% (avec Numba), le Julia et le R.
% \\


\subsection{Composants logiciels permettant la réalisation et la mise en forme des interfaces utilisateur}
\label{subsec:gui}


Les standards de conception des interfaces utilisateur pour les applications et
pages web sont bien établis. Le HTML (\textit{HyperText Markup Language})
\cite{html5_spec} constitue la norme en tant que langage de balisage pour la
représentation des pages web. Sa dernière révision majeure, le HTML5, inclut
l'ajout d'éléments et de balises supplémentaires, visant à améliorer et à
uniformiser la structure logique des interfaces web. \\


En complément au HTML, le CSS (\textit{Cascading Style Sheet}) est le standard reconnu
pour effectuer la mise en forme des documents HTML. Il fait d'ailleurs partie
intégrante de la spécification du HTML. Il permet de gérer des paramètres tels
les bordures, polices de caractère et l'alignement pour des blocs de classes ou
d'objets, ou encore au cas par cas pour des éléments individuels \cite{css}. \\



\subsection{Hébergement de l'application web}
\label{subsec:hebergement}
Le \textit{back-end} d'une application web doit être hébergé sur un serveur afin que les
usagers puissent l'utiliser, ce qui en facilite la diffusion et la mise à jour
tout en permettant le contrôle des utilisateurs et des privilèges. Sur la base
des spécifications logicielles, il importe que le système soit facile à
distribuer tout en ne nécessitant aucune installation ou configuration du côté
de l’utilisateur. Il existe des services d'hébergement en ligne, mais cette
option est écartée du fait qu'elle exige des compromis quant au niveau de
contrôle que les administrateurs peuvent avoir sur la machine. Un serveur
physique dédié et doté d'une puissance de calcul conséquente sera privilégié.
Évidemment, un tel serveur nécessite le choix d’un système d'exploitation adapté au contexte. Il en
existe plusieurs, tel qu’ Ubuntu Server, Microsoft Windows Server, CentOS
Server, etc. Une fois le système d'exploitation choisi, il ne reste qu'à le
configurer et créer l'environnement de travail.