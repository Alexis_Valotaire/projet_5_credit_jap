// Wait for load so you can use libs like JQuery
window.onload = function () {

    // Saved button
    var form;
    var data;

    // Seclect file and keep it in memory
    $('#list-tab a').click(function () {
        //var form = $(this).text();
        form = $('#select_file_form')[0];
        data = new FormData(form);
        data.append("selected_file", $(this).attr('name'));
    });

    // Delete scenario
    $('#delete_scenario_btn').click(function (event) {
        data.append('delete', 'true')
        event.preventDefault();
        // disabled the submit button
        $("#delete_scenario_btn").prop("disabled", true);
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "/",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {

				// Reload the page on the success
                location.reload();

                $("#delete_scenario_btn").prop("disabled", false);

            },
            error: function (e) {
                console.log("ERROR : ", e);
                $("#delete_scenario_btn").prop("disabled", false);
            }
        });
    });

    // Send selected file to backend
    $('#submit_btn').click(function (event) {
        data.append('delete', 'false')
        event.preventDefault();
        // disabled the submit button
        $("#submit_btn").prop("disabled", true);
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "/",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {

				// Reload the page on the success
                location.reload();

                $("#submit_btn").prop("disabled", false);

            },
            error: function (e) {

                // $("#result").text(e.responseText);
                console.log("ERROR : ", e);
                $("#submit_btn").prop("disabled", false);
            }
        });
    });

    // Run button
    $('#play_button').click(function () {
        data = new FormData(form);
        data.append("home_form_type", "run_optimization");
        $.ajax({
            type: "POST",
            url: "/run",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (result) {
                // Write result into html
                $('#display_result').html(result);
				// Reload the page on the success
                $("#play_button").prop("disabled", false);

            },
            error: function (e) {

                // $("#result").text(e.responseText);
                console.log("ERROR : ", e);
                $("#play_button").prop("disabled", false);
            }
        });
    });
};