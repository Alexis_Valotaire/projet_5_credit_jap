import datetime
import sqlalchemy as sa
import sqlalchemy.orm as orm

from optimizer.models.modelbase import SqlAlchemyBase


class UserFile(SqlAlchemyBase):
    __tablename__ = 'user_files'

    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    file_name = sa.Column(sa.String, index=True)
    nb_power_plant = sa.Column(sa.Integer, default=1)
    nb_period = sa.Column(sa.Integer, default=1)
    period_duration = sa.Column(sa.Integer, default=1)
    created_date = sa.Column(sa.DateTime, default=datetime.datetime.now, index=True)
    last_used = sa.Column(sa.DateTime, default=datetime.datetime.now, index=True)

    # Owner relationship
    owner_id = sa.Column(sa.Integer, sa.ForeignKey("users.id"))
    user_file = orm.relation('User')