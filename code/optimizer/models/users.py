import datetime
import sqlalchemy as sa
from typing import List
from optimizer.models.user_files import UserFile
from optimizer.models.modelbase import SqlAlchemyBase
import sqlalchemy.orm as orm


class User(SqlAlchemyBase):
    __tablename__ = 'users'

    id = sa.Column(sa.Integer, primary_key=True, index=True, autoincrement=True)
    first_name = sa.Column(sa.String, nullable=True)
    last_name = sa.Column(sa.String, nullable=True)
    email = sa.Column(sa.String, index=True, unique=True, nullable=True)
    password = sa.Column(sa.String, nullable=True, index=True)
    created_date = sa.Column(sa.DateTime, default=datetime.datetime.now, index=True)
    last_login = sa.Column(sa.DateTime, default=datetime.datetime.now, index=True)

    # Owner relationship
    files: List[UserFile] = orm.relation("UserFile", order_by=UserFile.id, back_populates='user_file')
