import os
import sys

# Change current folder to root
#  Server
# folder = os.path.abspath(os.path.join(os.path.dirname(__file__), '../../..'))
#  Local
folder = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, folder)

import infrastructure.optimisation as op
import infrastructure.parser as parser
import numpy as np

filePath = "../../data/scenario_po.xlsx"

scenario_info = parser.parse(filePath)

parser.printScenarioInfo(scenario_info)

Vit_borne = []

for i in range(len(scenario_info.listeCentrale)):
    temp = [scenario_info.listeCentrale[i].volMin, scenario_info.listeCentrale[i].volMax]
    Vit_borne.append(temp)

Qit_borne = []

for i in range(len(scenario_info.listeCentrale)):
    temp = [scenario_info.listeCentrale[i].debitMin, scenario_info.listeCentrale[i].debitMax]
    Qit_borne.append(temp)

Vinit = []
for i in range(len(scenario_info.listeCentrale)):
    temp = scenario_info.listeCentrale[i].volIni
    Vinit.append(temp)

# apports = []
# temp = []
#
# for j in range(scenario_info.nbPeriode):
#     for i in range(len(scenario_info.listeCentrale)):
#         temp.append(scenario_info.listeCentrale[i].listeApport[j])
#     apports.append(temp)
#     temp = []

apports = [[], []]

for j in range(scenario_info.nbPeriode):
    for i in range(len(scenario_info.listeCentrale)):
        apports[i].append(scenario_info.listeCentrale[i].listeApport[j])

coeff = []
for i in range(len(scenario_info.listeCentrale)):
    temp = scenario_info.listeCentrale[i].coefficients
    coeff.append(temp)

Vfin = []
for i in range(len(scenario_info.listeCentrale)):
    temp = scenario_info.listeCentrale[i].volFin
    Vfin.append(temp)

prob = op.probleme()

# test = np.asarray(Vit_borne)
# test1 = np.asarray(Qit_borne)
# test2 = np.asarray(Vinit)
# test3 = np.asarray(apports)
# test4 = np.asarray(Vfin)
# test5 = np.asarray(coeff)

result = prob.solveProblem(len(scenario_info.listeCentrale), scenario_info.nbPeriode, np.asarray(Vit_borne),
                           np.asarray(Qit_borne), np.asarray(Vinit), np.asarray(apports), int(scenario_info.duree),
                           scenario_info.factConversion, np.asarray(Vfin), np.asarray(coeff))
