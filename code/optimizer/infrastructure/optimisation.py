from pulp import *


class probleme(object):

    def __init__(self):
        # Creation d'une variable "prob" de type Linear-programming problem (pulp)
        # Parametres : Nom du probleme, Objectif desire (maximize ou minimize)
        self.prob = LpProblem("optimisation hydroelectricite", LpMaximize)

    # Fonction de resolution de probleme d'optimisation lineaire
    # Parametres : self, Nombre de centrales, Nombre de periodes, Bornes de volumes, Bornes de debits, Volumes initiaux,
    #			   Apports naturels, Duree, Facteur de conversion m^3/s en hm^3, Volumes finaux, Coefficients
    def solveProblem(self, NbCentrale, NbPeriode, Vit_borne, qit_borne, Vinit, apport, D, c, Vfin, C):

        # Creation des contraintes de volume et de debit
        Vit_Matrix = []
        qit_Matrix = []

        # t est un array de 0 a NbPeriode - 1
        for i in range(NbCentrale):

            Vit_Matrix.append([])
            qit_Matrix.append([])
            # i est un array de 0 a NbCentrale
            for t in range(NbPeriode):
                # Ajout d'une variable contrainte de volume et debit
                # En d'autres termes, on impose le volume min et max
                # a chaque periode pour chaque centrale
                # Parametres : Nom de la variable, Borne inferieur, Borne superieur
                # Ex : Pour la periode t = 0 et la centrale i = 0,
                # la contrainte est : Vit00 appartient a [Volume_min, Volume_max]
                Vit_Matrix[i].append(
                    LpVariable("vit" + "_" + str(t + 1) + "_" + str(i + 1), Vit_borne[i][0], Vit_borne[i][1]))
                qit_Matrix[i].append(
                    LpVariable("qit" + "_" + str(t + 1) + "_" + str(i + 1), qit_borne[i][0], qit_borne[i][1]))

        # Creation de la fonction objectif
        fonction_Objective = None

        # t est un array de 0 a NbPeriode - 1
        for t in range(NbPeriode):
            # i est un array de 0 a NbCentrale
            for i in range(NbCentrale):
                # La fonction objectif est la somme pour chaque centrale a chaque periode
                fonction_Objective += D * (Vit_Matrix[i][t] * C[i][0] + qit_Matrix[i][t] * C[i][1] + C[i][2])

        #  La fonction objective est ajoute au probleme
        self.prob += fonction_Objective

        # Creation de la contrainte de conservation de l'eau
        for i in range(NbCentrale):
            if i == 0:
                self.prob += Vit_Matrix[i][0] == Vinit[i]
                self.prob += Vit_Matrix[i][1] == Vinit[i] - qit_Matrix[i][0] * c * D + apport[i][0] * c * D
                for t in range(1, NbPeriode - 1):
                    self.prob += Vit_Matrix[i][t + 1] == Vit_Matrix[i][t] - qit_Matrix[i][t] * c + apport[i][
                        t] * c * D

                self.prob += Vit_Matrix[i][NbPeriode - 1] - qit_Matrix[i][NbPeriode - 1] * c * D + apport[i][
                    NbPeriode - 1] * c * D == Vfin[i]

            # Rest of centrale take previous apport
            else:
                self.prob += Vit_Matrix[i][0] == Vinit[i]
                self.prob += Vit_Matrix[i][1] == Vinit[i] - qit_Matrix[i][0] * c * D + apport[i][0] * c * D + \
                             qit_Matrix[i - 1][0] * c * D
                for t in range(1, NbPeriode - 1):
                    self.prob += Vit_Matrix[i][t + 1] == Vit_Matrix[i][t] - qit_Matrix[i][t] * c + apport[i][
                        t] * c * D + qit_Matrix[i - 1][t] * c * D

                self.prob += qit_Matrix[i - 1][NbPeriode - 1] * c * D + Vit_Matrix[i][NbPeriode - 1] - qit_Matrix[i][
                    NbPeriode - 1] * c * D + apport[i][NbPeriode - 1] * c * D == Vfin[i]

        self.x_data = []
        self.varValues = []

        try:
            self.prob.solve()
            # The status of the solution is printed to the screen
            self.prob.text = "Status:" + str(LpStatus[self.prob.status]) + "\n"
            # Each of the variables is printed with it's resolved optimum value
            fichier_test = open("test.txt", "w")
            a = self.prob.modifiedConstraints
            for i in range(len(a)):
                fichier_test.write(str(a[i]) + '\n')
            fichier_test.close()
            # print(self.prob.variables())
            for v in self.prob.variables():
                self.prob.text += str(v.name) + "=" + str(v.varValue) + "\n"
                self.varValues.append(v.varValue)

            # Need this to display graph
            i = 0
            for var in self.prob.variables():
                if i > NbPeriode - 1:
                    break
                else:
                    row_data = str(var.name).split('_')
                    if row_data[2] == '1':
                        period = int(row_data[1])
                        self.x_data.append(period)
                        i += 1

            # The optimised objective function value is printed to the screen
            self.prob.text += "L'energie totale  = " + str(value(self.prob.objective)) + "\n"
            # self.prob.writeLP("result_lp.txt")
            fichier_prob = open("resultat.txt", "w")
            fichier_prob.write(self.prob.text)

            # Need return self, not self.prob
            return self

        except Exception as e:
            print(e)
