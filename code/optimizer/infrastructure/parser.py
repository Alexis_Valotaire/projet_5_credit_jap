import os

import pandas as pd

class Scenario:
    def __init__(self):
        self.listeCentrale = []
        self.duree = None
        self.nbPeriode = None
        # Transformation de m3/s en hm3
        self.factConversion = 0.0036
    
class Centrale:
    def __init__(self):
        self.volIni = None
        self.volFin = None
        self.volMin = None
        self.volMax = None
        self.debitMin = None
        self.debitMax = None
        self.listeApport = []
        self.coefficients = []

def parse(file):
    df = None

    #try:
    if '.csv' in file:
        df = pd.read_csv(file, sep=';')
    elif '.xlsx' in file or '.xls' in file:
        df = pd.read_excel(file)
    else:
        print('Exception : Could not open file')

    # Recuperation de la duree d'une periode
    posx, posy = (df.applymap(lambda x: str(x).startswith('Duree/periode'))).values.nonzero()
    myScenario = Scenario()
    myScenario.duree = float(df.iat[posx[0] + 1, posy[0]])

    # Recuperation des coefficients
    posx, posy = (df.applymap(lambda x: str(x).startswith('Coefficients'))).values.nonzero()
    for i in range(len(posx)):
        newCentral = Centrale()
        c1 = float(df.iat[posx[i] + 1, posy[i]])
        c2 = float(df.iat[posx[i] + 2, posy[i]])
        c3 = float(df.iat[posx[i] + 3, posy[i]])

        newCentral.coefficients.append(c1)
        newCentral.coefficients.append(c2)
        newCentral.coefficients.append(c3)

        myScenario.listeCentrale.append(newCentral)

    # Recuperation des volumes initiaux
    posx, posy = (df.applymap(lambda x: str(x).startswith('Vol Ini'))).values.nonzero()
    for i in range(len(posx)):
        myScenario.listeCentrale[i].volIni = float(df.iat[posx[i] + 1, posy[i]])

    # Recuperation des volumes finaux
    posx, posy = (df.applymap(lambda x: str(x).startswith('Vol Final'))).values.nonzero()
    for i in range(len(posx)):
        myScenario.listeCentrale[i].volFin = float(df.iat[posx[i] + 1, posy[i]])

    # Recuperation des volumes minimals
    posx, posy = (df.applymap(lambda x: str(x).startswith('Vol Min'))).values.nonzero()
    for i in range(len(posx)):
        myScenario.listeCentrale[i].volMin = float(df.iat[posx[i] + 1, posy[i]])

    # Recuperation des volumes maximals
    posx, posy = (df.applymap(lambda x: str(x).startswith('Vol Max'))).values.nonzero()
    for i in range(len(posx)):
        myScenario.listeCentrale[i].volMax = float(df.iat[posx[i] + 1, posy[i]])

    # Recuperation des debits minimals
    posx, posy = (df.applymap(lambda x: str(x).startswith('Debit Min'))).values.nonzero()
    for i in range(len(posx)):
        myScenario.listeCentrale[i].debitMin = float(df.iat[posx[i] + 1, posy[i]])

    # Recuperation des debits maximals
    posx, posy = (df.applymap(lambda x: str(x).startswith('Debit Max'))).values.nonzero()
    for i in range(len(posx)):
        myScenario.listeCentrale[i].debitMax = float(df.iat[posx[i] + 1, posy[i]])

    # Recuperation des apports
    posx, posy = (df.applymap(lambda x: str(x).startswith('Apport'))).values.nonzero()

    listeApportTemp = []
    for i in range(len(posx)):
        j = 1
        while True:
            try:
                apport =  float(df.iat[posx[i] + j, posy[i]])
                if isNaN(apport):
                    break
                listeApportTemp.append(apport)
                myScenario.listeCentrale[i].listeApport.append(apport)
                j += 1
            except:
                break

    myScenario.nbPeriode = len(myScenario.listeCentrale[0].listeApport)

    return myScenario

def printScenarioInfo(scenario):
    print('****************************')
    print('Informations sur le scenario')
    print('****************************')
    print('')
    for i in range (len(scenario.listeCentrale)):
        print('Central #' + str(i+1))
        print('****************************')
        print('Volume initial et final : ' + str(scenario.listeCentrale[i].volIni) + ' et ' + str(scenario.listeCentrale[i].volFin))
        print('Bornes volume : ' + str(scenario.listeCentrale[i].volMin) + ' et ' + str(scenario.listeCentrale[i].volMax))
        print('Bornes debit : ' + str(scenario.listeCentrale[i].debitMin) + ' et ' + str(scenario.listeCentrale[i].debitMax))
        c = ''
        for j in range (len(scenario.listeCentrale[i].coefficients)):
            c += str(scenario.listeCentrale[i].coefficients[j]) + ' '
        print('Coefficients : ' + c)
        print("Nombre d'apports : " + str(len(scenario.listeCentrale[i].listeApport)))
        print('')
def isNaN(num):
    return num != num