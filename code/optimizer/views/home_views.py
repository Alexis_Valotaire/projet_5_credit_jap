from flask import render_template, Blueprint, redirect, session, send_file
from pulp import value

from viewmodels.home.download_file_view_model import DownloadFileViewModel
from viewmodels.home.index_view_model import IndexViewModel
from viewmodels.home.result_viewer_view_model import ResultViewerViewModel

blueprint = Blueprint('home', __name__, template_folder='templates')


@blueprint.route('/', methods=['GET'])
def index_get():
    # Default route
    vm = IndexViewModel()

    if not vm.user_id:
        return redirect("/account/login")

    return render_template('home/index.html', vm=vm, session=session)


@blueprint.route('/', methods=['POST'])
def index_post():
    vm = IndexViewModel()
    print(vm.request.form.get('delete_scenario'))
    if vm.request.form.get('home_form_type') == 'upload_file':

        vm.upload_user_file()

        # Failure
        if vm.error:
            return redirect("/")

        # Success
        return redirect("/")

    # If user clicks the "folder" icon to load an existing scenario previously
    # saved to his account
    elif vm.request.form.get('home_form_type') == 'saved_file':

        vm.select_saved_file()
        # Failure
        if vm.error:
            return redirect("/")

        # Success
        return redirect("/")

    vm.update_file_list()

    return redirect('/')


@blueprint.route('/run', methods=['POST'])
def result_viewer_post():
    vm = ResultViewerViewModel()
    if vm.error:
        return render_template('home/display_result.html', vm=vm, session=session)

    return render_template('home/display_result.html', resultat=vm.prob_result.prob.text,
                           energy=str(value(vm.prob_result.prob.objective)), nb_centrales=vm.nb_centrales,
                           plot=vm.figures, vm=vm)


@blueprint.route('/download_result', methods=['POST', 'GET'])
def download_result_post():
    # When user click on the save button
    vm = DownloadFileViewModel()

    if vm.error:
        return redirect('/')

    return send_file(vm.user_result_folder + 'results.zip', mimetype='zip',
                     attachment_filename='result.zip',
                     as_attachment=True)
