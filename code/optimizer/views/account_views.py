from flask import render_template, Blueprint, redirect, session, flash
from optimizer.services.user_services import create_user, login_user
from optimizer.services.user_files_services import get_file_count
import optimizer.infrastructure.cookie_auth as cookie_auth
from viewmodels.account.index_view_model import IndexViewModel
from viewmodels.account.login_view_model import LoginViewModel
from viewmodels.account.register_view_model import RegisterViewModel

blueprint = Blueprint('account', __name__, template_folder='templates')


@blueprint.route('/account', methods=['GET'])
def index_get():
    vm = IndexViewModel()

    # If no user found logged in
    if not vm.user:
        return redirect('/account/login')

    file_data = get_file_count(vm.user_id)

    return render_template('account/index.html', vm=vm.to_dict(), file_data=file_data)


@blueprint.route('/account', methods=['POST'])
def index_post():
    vm = IndexViewModel()

    vm.validate()

    if vm.error:
        return render_template('account/index.html', vm=vm.to_dict())

    vm.update_email(vm.user.email, vm.request.form.get('new_email'))

    if vm.error:
        return render_template('account/index.html', vm=vm.to_dict())

    return redirect('/account')


# LOGIN

@blueprint.route('/account/login', methods=['GET'])
def login_get():
    vm = RegisterViewModel()

    # If user found go to home index
    if vm.user_id:
        return redirect('/')

    return render_template('account/login.html', vm=vm.to_dict())


@blueprint.route('/account/login', methods=['POST'])
def login_post():
    vm = LoginViewModel()

    vm.validate()

    if vm.error:
        return render_template('account/login.html', vm=vm.to_dict())

    user = login_user(vm.email, vm.password)

    if not user:
        return render_template('account/login.html', vm=vm.to_dict())
    else:
        # Create cookie
        session.clear()
        resp = redirect('/')
        cookie_auth.set_auth(resp, user.id)

        return resp


# REGISTER

@blueprint.route('/account/register', methods=['GET'])
def register_get():
    vm = RegisterViewModel()
    return render_template('account/register.html', vm=vm.to_dict())


@blueprint.route('/account/register', methods=['POST'])
def register_post():
    vm = RegisterViewModel()

    vm.validate()

    if vm.error:
        return render_template('account/register.html', vm=vm.to_dict())

    user = create_user(vm.first_name, vm.last_name, vm.email, vm.password)

    if not user:
        return render_template('account/register.html', vm=vm.to_dict())

    vm.create_upload_folder()

    # Create cookie
    session.clear()
    resp = redirect('/')
    user = vm.get_user_id()
    cookie_auth.set_auth(resp, user.id)
    print(vm.get_user_id())

    flash("Compte créé avec succès!")

    return resp


# FORGOT PASSWORD
@blueprint.route('/account/forgot_password', methods=['GET'])
def forgot_password_get():
    return render_template('account/forgot_password.html')


@blueprint.route('/account/forgot_password', methods=['POST'])
def forgot_password_post():
    return render_template('account/forgot_password.html')

# LOGOUT

@blueprint.route('/account/logout')
def logout():
    resp = redirect('/account/login')
    cookie_auth.logout(resp)

    return resp
