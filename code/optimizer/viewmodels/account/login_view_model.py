from viewmodels.shared.viewmodel_init_user import ViewModelBase
from optimizer.services import user_services

class LoginViewModel(ViewModelBase):
    def __init__(self):
        super().__init__()
        self.email = self.request.form.get('email', '').lower().strip()
        self.password = self.request.form.get('password', '').strip()

    def validate(self):
        if not self.email:
            self.error = "Veuillez entrer une adresse courriel"
        elif not self.password:
            self.error = "Veuillez entrer un mot de passe"
        elif not user_services.find_user_by_email(self.email):
            self.error = "Ce compte n'existe pas"