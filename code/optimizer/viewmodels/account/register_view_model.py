import os

from services.user_services import find_user_by_email
from viewmodels.shared.viewmodel_init_user import ViewModelBase
from optimizer.services import user_services

class RegisterViewModel(ViewModelBase):
    def __init__(self):
        super().__init__()
        self.first_name = self.request.form.get('first_name')
        self.last_name = self.request.form.get('last_name')
        self.email = self.request.form.get('email', '').lower().strip()
        self.password = self.request.form.get('password', '').strip()
        self.repeated_password = self.request.form.get('repeated_password', '').strip()

    def validate(self):
        if not self.first_name or not self.last_name or not self.email or not self.password or not self.repeated_password:
            self.error = "Il manque certains paramètres"

        if self.repeated_password != self.password:
            self.error = "Vous n'avez pas entré le même mot de passe"

        if user_services.find_user_by_email(self.email):
            self.error = "Ce courriel a déjà été utilisé"

    def create_upload_folder(self):
        user_folder = "./uploaded_files/" + str(self.email)

        if not os.path.exists(user_folder):
            os.makedirs(user_folder)

        user_result_folder = "./uploaded_files/" + str(self.email) + "/tmp_result"
        if not os.path.exists(user_result_folder):
            os.makedirs(user_result_folder)

    def get_user_id(self):
        return find_user_by_email(self.email)
