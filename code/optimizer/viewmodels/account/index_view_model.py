import os

from services import user_services
from services.user_services import find_user_by_id, update_email

from viewmodels.shared.viewmodel_init_user import ViewModelBase


class IndexViewModel(ViewModelBase):
    def __init__(self):
        super().__init__()
        self.user = find_user_by_id(self.user_id)

    def validate(self):

        if not self.request.form.get('new_email').strip():
            self.error = "Veuillez entrer un adresse courriel"
        elif user_services.find_user_by_email(self.request.form.get('new_email')):
            self.error = "Cette adresse est déjà utilisé"

    def update_email(self, email, new_email):
        self.user = update_email(email, new_email)
        if not self.user:
            self.error = "Une erreur est survenue"
        else:
            os.chdir('./uploaded_files')
            user_folder = str(email)
            new_user_folder = str(new_email)
            os.rename(user_folder, new_email)
