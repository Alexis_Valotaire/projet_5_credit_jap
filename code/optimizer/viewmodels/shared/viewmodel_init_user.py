from typing import Optional

import bson
import flask
from flask import Request

from optimizer.infrastructure import cookie_auth


class ViewModelBase:
    def __init__(self):
        self.request: Request = flask.request

        self.error: Optional[str] = None
        self.success: Optional[str] = None
        self.message: Optional[str] = None

        self.user_id: Optional[bson.ObjectId] = cookie_auth.get_user_id_via_auth_cookie(self.request)

    def to_dict(self):
        return self.__dict__