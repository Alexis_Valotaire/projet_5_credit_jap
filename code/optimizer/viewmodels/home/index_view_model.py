import os

from flask import session, flash
from werkzeug.utils import secure_filename

from services.user_files_services import get_user_files_by_user_id, create_user_file, \
    get_user_file_by_filename_and_user_id, delete_user_file
from services.user_services import find_user_by_id
from viewmodels.shared.viewmodel_init_user import ViewModelBase
import infrastructure.parser as parser

ALLOWED_EXTENSIONS = {'csv', 'xls', 'xlsx'}


class IndexViewModel(ViewModelBase):
    def __init__(self):
        super().__init__()
        if not session:
            session['user_file_id'] = None
            session['user_file_name'] = None
            session['nb_power_plant'] = None
            session['period_number'] = None
            session['period_duration'] = None

        self.user = find_user_by_id(self.user_id)

        # If user logged
        if self.user:
            self.input_file = self.request.form.get('upload_file')

            # Get user files
            self.user_files = get_user_files_by_user_id(self.user_id)

            # User files attributes
            self.user_folder = './uploaded_files/' + str(self.user.email)

            # parser output
            self.scenario_info = None

    def allowed_file(self, filename):
        return '.' in filename and \
               filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

    def update_file_list(self):
        # Format file name to remove .csv at the end
        for index in range(len(self.user_files)):
            self.user_files[index].file_name = os.path.splitext(self.user_files[index].file_name)[0]

    def upload_user_file(self):

        if 'power_plant_input' not in self.request.files:
            self.error = "Aucune partie de fichier"
            flash(self.error)
            return
        file = self.request.files['power_plant_input']

        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            self.error = "Aucun fichier de sélectionné"
            flash(self.error)
            return

        if not self.allowed_file(file.filename):
            self.error = "Vous devez entrez un fichier .csv ou .xlsx"
            flash(self.error)
            return

        # Is the file uploaded?
        if file:
            # Check if exec of code, if so, convert it into something non-executable
            filename = secure_filename(file.filename)
            user_folder = "./uploaded_files/" + str(self.user.email)

            if not os.path.exists(user_folder):
                os.makedirs(user_folder)

            file.save(os.path.join(user_folder, filename))
            file_path = user_folder + "/" + filename

            self.get_uploaded_scenario_info(file_path)

            #  Get scenario data from parser
            nb_power_plant = len(self.scenario_info.listeCentrale)
            nb_period = len(self.scenario_info.listeCentrale[0].listeApport)
            period_duration = 1

            if create_user_file(filename, self.user_id, nb_power_plant, nb_period, period_duration):
                session.pop('_flashes', None)
                self.success = "Fichier importé avec succès!"
                flash(self.success)
                return
            else:
                self.error = "Une erreur est survenue"
                flash(self.error)
                return
        else:
            self.error = "Une erreur est survenue"
            flash(self.error)
            return

    def select_saved_file(self):
        selected_file_data_raw = str(self.request.form.get('selected_file'))
        delete_option = str(self.request.form.get('delete'))

        # Raw data has format: filename.nb_centrales.nb_periodes.duree_periode
        # So we need to extract each parameter
        selected_file_data = selected_file_data_raw.split('.')

        selected_file = selected_file_data[0] + "." + selected_file_data[1]
        user_file = get_user_file_by_filename_and_user_id(self.user_id, secure_filename(selected_file))

        if not user_file:
            self.error = "Une erreur est survenue"
            flash(self.error)
            return

        # If Delete is pressed remove, remove file and db entry
        if delete_option == 'true':
            if user_file:
                # Delete entry
                delete_user_file(self.user.id, user_file.file_name)

                # Delete file
                os.remove(self.user_folder + "/" + user_file.file_name)

                # Clear session so current scenario is no longer there
                if session['user_file_name'] == user_file.file_name:
                    session.clear()
                    flash("Fichier supprimé avec succès!")

                return

        session['user_file_name'] = user_file.file_name
        session['nb_power_plant'] = user_file.nb_power_plant
        session['period_number'] = user_file.nb_period
        session['period_duration'] = user_file.period_duration

        return

    def get_uploaded_scenario_info(self, file_path):
        # Call data extraction
        try:
            self.scenario_info = parser.parse(file_path)

        except:
            self.error = "Mauvais format de scénario"
            flash(self.error)
        return