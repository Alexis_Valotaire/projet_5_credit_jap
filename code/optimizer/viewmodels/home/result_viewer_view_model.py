import os

from flask import session, flash
# from infrastructure.parser_test import *
import plotly.graph_objs as go
from plotly.subplots import make_subplots
import plotly
import pandas as pd
import json

import infrastructure.optimisation as op
import infrastructure.parser as parser
import numpy as np

from services.user_files_services import get_user_files_by_user_id
from services.user_services import find_user_by_id
from viewmodels.shared.viewmodel_init_user import ViewModelBase


class ResultViewerViewModel(ViewModelBase):
    def __init__(self):
        super().__init__()

        # Get current user
        self.user = find_user_by_id(self.user_id)

        # If user logged
        if self.user and session['user_file_name']:

            self.titre = "Paramètres et résultats de l'optimisation"
            self.titre3 = "Résultats"
            # self.file = self.request.form.get('nom_file')
            self.file = "./uploaded_files/" + self.user.email + "/" + session["user_file_name"]
            self.nb_centrales = session["nb_power_plant"]
            self.nb_periodes = session["period_number"]
            self.duree_periode = session["period_duration"]

            # Get user files
            self.user_files = get_user_files_by_user_id(self.user_id)

            # User files attributes
            self.user_folder = './uploaded_files/' + str(self.user.email)

            # Opti problems
            self.parameters = None
            self.results = None
            self.scenario_info = None

            self.total_energy = None

            self.prob = op.probleme()

            self.parse_file()

            self.optimize_problem()
        else:
            self.error = "Veuillez choisir un scénario"
            flash(self.error)
            return

    def parse_file(self):
        # Call data extraction
        try:
            self.scenario_info = parser.parse(self.file)

        except:
            self.error = "Mauvais format de scénario"
            flash(self.error)
            return

        self.Vit_borne = []

        for i in range(len(self.scenario_info.listeCentrale)):
            temp = [self.scenario_info.listeCentrale[i].volMin, self.scenario_info.listeCentrale[i].volMax]
            self.Vit_borne.append(temp)

        self.Qit_borne = []

        for i in range(len(self.scenario_info.listeCentrale)):
            temp = [self.scenario_info.listeCentrale[i].debitMin, self.scenario_info.listeCentrale[i].debitMax]
            self.Qit_borne.append(temp)

        self.Vinit = []
        for i in range(len(self.scenario_info.listeCentrale)):
            temp = self.scenario_info.listeCentrale[i].volIni
            self.Vinit.append(temp)

        self.apports = []

        temp_nb_centrale = len(self.scenario_info.listeCentrale)
        for centrale in range(temp_nb_centrale):
            self.apports.append([])
            for periode in range(len(self.scenario_info.listeCentrale[centrale].listeApport)):
                self.apports[centrale].append(self.scenario_info.listeCentrale[centrale].listeApport[periode])

        self.coeff = []
        for i in range(len(self.scenario_info.listeCentrale)):
            temp = self.scenario_info.listeCentrale[i].coefficients
            self.coeff.append(temp)

        self.Vfin = []
        for i in range(len(self.scenario_info.listeCentrale)):
            temp = self.scenario_info.listeCentrale[i].volFin
            self.Vfin.append(temp)

    def optimize_problem(self):
        # Call data processing
        self.prob_result = self.prob.solveProblem(len(self.scenario_info.listeCentrale),
                                                  self.scenario_info.nbPeriode, np.asarray(self.Vit_borne),
                                                  np.asarray(self.Qit_borne), np.asarray(self.Vinit),
                                                  np.asarray(self.apports), int(self.scenario_info.duree),
                                                  self.scenario_info.factConversion, np.asarray(self.Vfin),
                                                  np.asarray(self.coeff))

        # Call data formatting for visualization
        # self.figures = self.formatData(self.this_problem_solution, self.nb_centrales)
        self.figures = self.formatData(self.prob_result, len(self.scenario_info.listeCentrale))

    # And plot data
    def formatData(self, problemSolution, nbCentrales):
        # Separate qit and vit and get result
        this_problem_solution_debits = []
        this_problem_solution_volumes = []

        # Get vit and qit
        for index in problemSolution.prob.variables():
            if index.name.lower().startswith("vit"):
                this_problem_solution_volumes.append(index)

            if index.name.lower().startswith("qit"):
                this_problem_solution_debits.append(index)

        # Create vectors for volume/debits for each central
        volume_results = []
        debit_results = []

        #  Get volume and debit data into 2d list [centrale][volume or debit]
        for centrale in range(nbCentrales):
            volume_results.append([])
            debit_results.append([])
            for index in this_problem_solution_volumes:
                if index.name.lower().endswith(str(centrale + 1)):
                    volume_results[centrale].append((index.varValue / self.Vit_borne[centrale][1]) * 100)

            for index in this_problem_solution_debits:
                if index.name.lower().endswith(str(centrale + 1)):
                    debit_results[centrale].append((index.varValue / self.Qit_borne[centrale][1]) * 100)

        # Init plot data for centrales
        df_vData = []
        df_qData = []
        figure = []

        # Set x and y data for volume/debit plots
        for centrale in range(nbCentrales):
            df_vData.append({'Periode': problemSolution.x_data, 'Volume': volume_results[centrale]})
            df_qData.append({'Periode': problemSolution.x_data, 'Debit': debit_results[centrale]})

        # Create plot vector
        for i in range(int(nbCentrales)):
            figure.append(self.create_v_Plot(df_vData[i], i + 1))
            figure.append(self.create_q_Plot(df_qData[i], i + 1))

        return figure

    def create_v_Plot(self, df_vdata, central_no):
        # Create double y-axis plots for volume and debit
        # NOTE: maybe useless if our problems always contain only one
        # "variable de décision"

        user_result_folder = "./uploaded_files/" + str(self.user.email) + "/tmp_result"
        if not os.path.exists(user_result_folder):
            os.makedirs(user_result_folder)

        file_name = user_result_folder + '/volume_centrale_' + str(central_no) + '.csv'
        # Create DataFrame from data dicts
        try:
            v_dframe = pd.DataFrame(df_vdata)
            v_dframe.to_csv(file_name, sep='\t', encoding='utf-8')
        except:
            self.error = "Le nombre de données de volumes ne correspond pas"
            flash(self.error)
            return

        v_dframe = v_dframe.sort_values(by=['Periode'])

        # Create main plot with 2 'y' axes
        fig = make_subplots()
        # Add traces to plot
        fig.add_trace(
            go.Scatter(
                x=v_dframe['Periode'],
                y=v_dframe['Volume'],
                name="Volumes"),
        )

        # Set x-axis title
        fig.update_xaxes(title_text="Périodes (h)")

        # Set y-axes titles
        fig.update_yaxes(title_text="<b>Pourcentage du Volume Max (hm<sup>3</sup>)</b>")

        # Jsonify and return this shit
        graphJSON = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)
        return graphJSON

    def create_q_Plot(self, df_qdata, central_no):
        # Create double y-axis plots for volume and debit
        # NOTE: maybe useless if our problems always contain only one
        # "variable de décision"

        user_result_folder = "./uploaded_files/" + str(self.user.email) + "/tmp_result"
        if not os.path.exists(user_result_folder):
            os.makedirs(user_result_folder)

        file_name = user_result_folder + '/debit_centrale_' + str(central_no) + '.csv'
        # Create DataFrame from data dicts
        try:
            q_dframe = pd.DataFrame(df_qdata)
            q_dframe.to_csv(file_name, sep='\t', encoding='utf-8')
        except:
            self.error = "Le nombre de données de débits ne correspond pas"
            flash(self.error)
            return

        q_dframe = q_dframe.sort_values(by=['Periode'])
        # Create main plot with 2 'y' axes
        fig = make_subplots()
        # Add traces to plot
        fig.add_trace(
            go.Scatter(
                x=q_dframe['Periode'],
                y=q_dframe['Debit'],
                name="Debits",
                marker_color='rgba(255, 0, 0, 1)'),
        )

        # Set x-axis title
        fig.update_xaxes(title_text="Périodes (h)")

        # Set y-axes titles
        fig.update_yaxes(title_text="<b>Pourcentage du Débit Max (m<sup>3</sup>/s)</b>")

        # Jsonify and return this shit
        graphJSON = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)
        return graphJSON