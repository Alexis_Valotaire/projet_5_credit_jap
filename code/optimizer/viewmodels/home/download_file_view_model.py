import os
import zipfile

from services.user_services import find_user_by_id
from viewmodels.shared.viewmodel_init_user import ViewModelBase


class DownloadFileViewModel(ViewModelBase):
    def __init__(self):
        super().__init__()

        # Get current user
        self.user = find_user_by_id(self.user_id)

        # If user logged
        if self.user:
            # User files attributes
            self.user_result_folder = './uploaded_files/' + str(self.user.email) + '/tmp_result/'
            self.zip()
            return

    def zip(self):
        zip_file = zipfile.ZipFile(self.user_result_folder + 'results.zip', 'w', zipfile.ZIP_DEFLATED)

        # Zip all file in user result folder
        for root, dirs, files in os.walk(self.user_result_folder):
            for file in files:
                if 'results.zip' not in file:
                    zip_file.write(self.user_result_folder + file)
        zip_file.close()

        return
