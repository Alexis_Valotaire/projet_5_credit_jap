from typing import Optional

from passlib.handlers.sha2_crypt import sha512_crypt as crypto
import optimizer.models.db_session as db_session
from optimizer.models.users import User


def get_user_count() -> int:
    session = db_session.create_session()
    try:
        return session.query(User).count()
    finally:
        session.close()


def find_user_by_email(email: str) -> Optional[User]:
    session = db_session.create_session()
    try:
        return session.query(User).filter(User.email == email).first()
    finally:
        session.close()


def create_user(first_name: str, last_name: str, email: str, password: str) -> Optional[User]:
    if find_user_by_email(email):
        return None

    user = User()
    user.email = email
    user.first_name = first_name
    user.last_name = last_name
    user.password = hash_text(password)

    session = db_session.create_session()

    try:
        session.add(user)
        session.commit()
    finally:
        session.close()

    return user


def hash_text(text: str) -> str:
    hashed_text = crypto.encrypt(text, rounds=171204)
    return hashed_text


def update_email(old_email: str, new_email: str) -> Optional[User]:
    session = db_session.create_session()
    user = session.query(User).filter(User.email == old_email).first()

    user.email = new_email
    try:
        session.commit()
    finally:
        session.close()
    return user


def verify_hash(hashed_text: str, plain_text: str) -> bool:
    return crypto.verify(plain_text, hashed_text)


def login_user(email: str, password: str) -> Optional[User]:
    session = db_session.create_session()

    try:
        user = session.query(User).filter(User.email == email).first()
        if not user:
            return None

        if not verify_hash(user.password, password):
            return None

        return user
    finally:
        session.close()


def find_user_by_id(user_id: int) -> Optional[User]:
    session = db_session.create_session()
    try:
        user = session.query(User).filter(User.id == user_id).first()
    finally:
        session.close()

    return user
