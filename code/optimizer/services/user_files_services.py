import os
from typing import Optional, List
import datetime

import optimizer.models.db_session as db_session
from optimizer.models.user_files import UserFile


def get_file_count(user_id: int) -> int:
    session = db_session.create_session()
    try:
        return session.query(UserFile).filter(UserFile.owner_id == user_id).count()
    finally:
        session.close()


def find_file_by_id(file_id: int) -> Optional[UserFile]:
    session = db_session.create_session()
    try:
        user_file = session.query(UserFile).filter(UserFile.id == file_id).first()
        return user_file
    finally:
        session.close()


def find_file_by_name(file_name: str) -> Optional[UserFile]:
    session = db_session.create_session()
    try:
        return session.query(UserFile).filter(UserFile.file_name == file_name).first()
    finally:
        session.close()


def find_file_by_owner_id(owner_id: int) -> Optional[UserFile]:
    session = db_session.create_session()
    try:
        user_file = session.query(UserFile).filter(UserFile.owner_id == owner_id).first()
    finally:
        session.close()

    return user_file

def get_user_files_by_user_id(user_id: int) -> List[UserFile]:
    session = db_session.create_session()
    try:
        user_files = session.query(UserFile).filter(UserFile.owner_id == user_id).all()
    finally:
        session.close()

    return user_files

def get_user_file_by_filename_and_user_id(user_id: int, file_name : str) -> UserFile:
    session = db_session.create_session()
    try:
        user_file = session.query(UserFile).filter(UserFile.owner_id == user_id, UserFile.file_name.like(file_name + "%")).first()

    finally:
        session.close()

    return user_file


def create_user_file(file_name: str, owner_id: int, nb_power_plant: int, nb_period: int, period_duration: int) -> \
        Optional[UserFile]:
    session = db_session.create_session()

    if find_file_by_name(file_name) and find_file_by_owner_id(owner_id):

        user_file = session.query(UserFile).filter(UserFile.owner_id == owner_id).first()

        user_file.file_name = file_name
        user_file.nb_power_plant = nb_power_plant
        user_file.nb_period = nb_period
        user_file.period_duration = period_duration
        user_file.last_used = datetime.datetime.now()

        try:
            session.commit()
        finally:
            session.close()
    else:
        user_file = UserFile()
        user_file.file_name = file_name
        user_file.nb_power_plant = nb_power_plant
        user_file.nb_period = nb_period
        user_file.period_duration = period_duration
        user_file.owner_id = owner_id

        try:
            session.add(user_file)
            session.commit()
        finally:
            session.close()

    return user_file

# TODO finish delete user file
def delete_user_file(user_id, file_name):
    user_file = get_user_file_by_filename_and_user_id(user_id, file_name)
    session = db_session.create_session()

    try:
        session.delete(user_file)
        session.commit()
    finally:
        session.close()

    return