#!/usr/bin/python
# -*- coding: latin-1 -*-

from flask import Flask, request, render_template
import re
from parser_test import *
import plotly.express as px
import plotly.graph_objs as go
from plotly.subplots import make_subplots
import plotly
import pandas as pd
import json

app = Flask(__name__)

# Basic default route
@app.route('/', methods = ['GET', 'POST'])
def accueil():
	# Data processing
	if request.method == 'POST':
		titre = "Param�tres et r�sultats de l'optimisation"
		titre3 = "R�sultats"
		file = request.form.get('nom_file')
		nb_centrales = request.form.get('nb_cent')
		nb_periodes = request.form.get('nb_per')
		duree_periode = request.form.get('duree')
        
        # Call data init
		this_problem_init = initialisation(file, nb_centrales, nb_periodes, duree_periode)

		# Call data extraction
		try:
			this_problem_data = this_problem_init.extractData()

		except:
			flash("Data cannot be extracted, wrong format")
		
		# Call data processing
		this_problem_solution = processData(this_problem_data)

		# Call data formatting for visualization
		figures = formatData(this_problem_solution, nb_centrales)

		return render_template('display.html', titre = titre, parametres = this_problem_data.input, titre3 = titre3, resultat = this_problem_solution.prob.text, nb_centrales = int(nb_centrales), plot = figures)
		
	else :
		titre = "Programme d'optimisation hydro�lectrique"
		titre3 = "Saisir les informations:"
		return render_template('display.html', titre = titre, titre3 = titre3)


@app.errorhandler(404)
# Error 404 management for any invalid GET request
def myErrorHandle(e):
	titre = "Erreur 404!  Page non trouv�e!"
	return render_template('display.html', titre = titre)


def processData(problemData):
	# Convert to LpProblem and call the solver

	try:
		this_problem = probleme(problemData.NbCentrale, problemData.NbPeriode, problemData.Vit_borne, problemData.qit_borne, problemData.Vinit,
                        problemData.apport, problemData.D, problemData.c, problemData.Vfin, problemData.C)
	except:
		flash("Data cannot be converted to LpProblem object")

	try:
		this_problem_solution = this_problem.solveProblem(problemData.NbCentrale, problemData.NbPeriode, problemData.Vit_borne, problemData.qit_borne, problemData.Vinit,
                       problemData.apport, problemData.D, problemData.c, problemData.Vfin, problemData.C)
	except:
		flash("An error occured while attempting to solve the problem")

	return this_problem_solution


def formatData(problemSolution, nbCentrales):
	# Separate qit and vit
	this_problem_solution_volumes = [idx for idx in problemSolution.prob.variables() if idx.name.lower().startswith("vit")]

	for i in range(3):
		temp = this_problem_solution_volumes[0]
		this_problem_solution_volumes.remove(this_problem_solution_volumes[0])
		this_problem_solution_volumes.append(temp)

	this_problem_solution_debits = [idx for idx in problemSolution.prob.variables() if idx.name.lower().startswith("qit")]

	for i in range(3):
		temp = this_problem_solution_debits[0]
		this_problem_solution_debits.remove(this_problem_solution_debits[0])
		this_problem_solution_debits.append(temp)

	# Create vectors for volume/debits for each centrale
	this_problem_solution_volumes_c1 = [idx.varValue for idx in this_problem_solution_volumes if idx.name.lower().endswith("1")]
	this_problem_solution_volumes_c2 = [idx.varValue for idx in this_problem_solution_volumes if idx.name.lower().endswith("2")]
	this_problem_solution_volumes_c3 = [idx.varValue for idx in this_problem_solution_volumes if idx.name.lower().endswith("3")]
	this_problem_solution_debits_c1 = [idx.varValue for idx in this_problem_solution_debits if idx.name.lower().endswith("1")]
	this_problem_solution_debits_c2 = [idx.varValue for idx in this_problem_solution_debits if idx.name.lower().endswith("2")]
	this_problem_solution_debits_c3 = [idx.varValue for idx in this_problem_solution_debits if idx.name.lower().endswith("3")]

	# Init plot data for centrales
	df_vData = []
	df_qData = []
	figure = []
	
	# Set x and y data for volume/debit plots
	df_vData.append({'Periode': problemSolution.x_data, 'Volume': this_problem_solution_volumes_c1})
	df_vData.append({'Periode': problemSolution.x_data, 'Volume': this_problem_solution_volumes_c2})
	df_vData.append({'Periode': problemSolution.x_data, 'Volume': this_problem_solution_volumes_c3})
	df_qData.append({'Periode': problemSolution.x_data, 'Debit': this_problem_solution_debits_c1})
	df_qData.append({'Periode': problemSolution.x_data, 'Debit': this_problem_solution_debits_c2})
	df_qData.append({'Periode': problemSolution.x_data, 'Debit': this_problem_solution_debits_c3})

	# Create plot vector
	for i in range(int(nbCentrales)):
		figure.append(createPlot(df_vData[i], df_qData[i]))

	return figure

def createPlot(df_vdata, df_qdata):
	# Create double y-axis plots for volume and debit
	# NOTE: maybe useless if our problems always contain only one
	# "variable de d�cision"

	# Create DataFrame from data dicts
	v_dframe = pd.DataFrame(df_vdata)
	q_dframe = pd.DataFrame(df_qdata)
	# Create main plot with 2 'y' axes
	fig = make_subplots(specs=[[{"secondary_y": True}]])
	# Add traces to plot
	fig.add_trace(
		go.Scatter(
			x = v_dframe['Periode'],
			y = v_dframe['Volume'],
			name = "Volumes"),
		secondary_y = False,
	)
	fig.add_trace(
		go.Scatter(
			x = q_dframe['Periode'],
			y = q_dframe['Debit'],
			name = "Debits"),
		secondary_y = True,
	)
	# Add figure title
	fig.update_layout(
	    title_text="Volumes et d�bits en fonction des p�riodes"
	)

	# Set x-axis title
	fig.update_xaxes(title_text="P�riode")

	# Set y-axes titles
	fig.update_yaxes(title_text="<b>Volume</b>", secondary_y=False)
	fig.update_yaxes(title_text="<b>D�bit</b>", secondary_y=True)
	
	# Jsonify and return this shit
	graphJSON = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)
	return graphJSON
	
if __name__ == '__main__':
	app.run(debug = True)