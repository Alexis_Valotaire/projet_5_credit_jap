import numpy as np
#from infrastructure.optimisation import *
#test console js
from optimisation_test import *
import sys

# fichier = open("fichierInitialisation.txt", "r")

# temp = (fichier.readline()).split()
# NbCentrale = int(temp[0])
# temp = (fichier.readline()).split()
# NbPeriode = int(temp[0])

# ∆t
# temp = (fichier.readline()).split()
# D = int(temp[0])

class initialisation():

    def __init__(self, fichierTemp, NbCentraleTemp, NbPeriodeTemp, Duree):
        self.fichier = open(fichierTemp, "r")
        self.NbCentrale = int(NbCentraleTemp)
        self.NbPeriode = int(NbPeriodeTemp)
        self.D = int(Duree)
        return

    def extractData(self):

        # ∆t
        # temp = (fichier.readline()).split()
        # D = int(temp[0])
        # Cix = coefficient x dans la formule d’énergie de calcul d’énergie en MWH de chaque centrale i
        self.fichier.readline()
        self.C = np.zeros((self.NbCentrale, 3))
        for i in range(self.NbCentrale):
            temp = (self.fichier.readline()).split()
            for x in range(3):
                self.C[i][x] = float(temp[x])

        # CONTRAINTES VOLUME INITIAL (hm3)
        self.fichier.readline()
        self.Vinit = np.zeros(3)
        for i in range(self.NbCentrale):
            temp = (self.fichier.readline()).split()
            self.Vinit[i] = float(temp[0])

        # CONTRAINTES VOLUME FINAL (hm3)
        # Vfin = np.zeros(3)
        self.Vfin = self.Vinit

        # les bornes de volumes
        self.fichier.readline()
        self.Vit_borne = np.zeros((self.NbCentrale, 2))
        for i in range(self.NbCentrale):
            temp = (self.fichier.readline()).split()
            for x in range(2):
                self.Vit_borne[i][x] = float(temp[x])

        # les bornes de débit
        self.fichier.readline()
        self.qit_borne = np.zeros((self.NbCentrale, 2))
        for i in range(self.NbCentrale):
            temp = (self.fichier.readline()).split()
            for x in range(2):
                self.qit_borne[i][x] = float(temp[x])

        # apports (m3/s)
        self.fichier.readline()
        self.apport = np.zeros((self.NbPeriode, self.NbCentrale))

        for t in range(self.NbPeriode):
            temp = (self.fichier.readline()).split()
            for i in range(self.NbCentrale):
                self.apport[t][i] = float(temp[i])

        # transformation de m3/s en hm3
        self.c = 0.0864

        self.input = ""
        self.input += ("Nombre de centrale = " + str(self.NbCentrale) + "\nNombre de periode = " + str(self.NbPeriode))
        self.input += ("Borne du volume = " + str(self.Vit_borne) + "\nBorne du debit = \n" + str(self.qit_borne))
        self.input += ("Volume initial = " + str(self.Vinit) + "\nVolume final = " + str(self.Vfin))
        self.input += ("Apports = \n" + str(self.apport))
        self.input += ("Duree = " + str(self.D) + "\nc = " + str(self.c))
        self.input += ("Coefficients = \n" + str(self.C))

        #self.myProblem = probleme(self.NbCentrale, self.NbPeriode, self.Vit_borne, self.qit_borne, self.Vinit,
        #                          self.apport, self.D, self.c, self.Vfin, self.C)
        #self.solution = self.myProblem.solveProblem(self.NbCentrale, self.NbPeriode, self.Vit_borne, self.qit_borne,
        #                                           self.Vinit, self.apport, self.D, self.c, self.Vfin, self.C)

        #test js
        #print(self.input)
        #print(self.myProblem)
        #print("\n\nZE SOLUTION IS: \n\n")
        # if self.solution:
        #     print(self.solution.text)
        # else:
        #     print("Aucune solution retournée")

        return self

# if __name__ == '__main__':
#     myInit = initialisation(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
#     myInit.extractData()
