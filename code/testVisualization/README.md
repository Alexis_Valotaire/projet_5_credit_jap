TO USE THE "PROOF-OF-CONCEPT" GRADE VISUALIZATION TOOL:

- ** IMPORTANT: The content of folder "testVisualization" is autosufficient (nothing to do with the main app!!) **

- Reinstall requirements.txt (updated w/ pandas and plotly)
- Launch flaskServer.py
- This program uses the previous input file format, as seen in "init.txt"
- Default values have been inserted into html form for simplicity
- Hit the button and see the data, graphics and results