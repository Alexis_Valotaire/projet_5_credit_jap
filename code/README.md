### Directory explanation  

├── app.py  **--main file starting flask and init other**  
├── model  **--Design databases and tables**   
├── db  **--Contains databases**  
├── infrastructure  **--Useful scripts**  
├── services  **--Functions to get or update data in DB**  
├── templates  **--HTML**  
├── uploaded_file  **--Folder to get uploaded files**  
├── viewmodels  **--Validation, logics, processing**  
└── views  **--Routing**  

### Alembic
* alembic revision --autogenerate -m 'NOTES' -Pour commit les changements à la BD  
* alembic upgrade head -Pour update la BD


### Useful ressource  

* [unsplash](https://unsplash.com/) - Free stock images  
* [fontawesome](https://fontawesome.com/) - Free icons  
* [startbootsrap](https://startbootstrap.com/themes/) - Free/Paid Bootstrap Templates & Themes  