\documentclass[../ingenierie_detaillee.tex]{subfiles}


\section{Introduction et mise en contexte du projet} 
\label{sec:intro}
À l’ère de l’industrie 4.0, les entreprises qui sauront maximiser l’utilisation
des possibilités offertes par les nouvelles technologies bénéficieront
d’avantages concurrentiels majeurs essentiels à leur quête d’excellence. La
collecte de quantités massives de données touchant pratiquement tous les aspects
des procédés industriels étant plus accessible que jamais, le développement de
solutions visant à analyser et contextualiser ces données pour en extraire les
informations pertinentes et les représenter sous une forme intelligible pour les
décideurs de l’industrie revêt un degré d’importance sans précédent. C’est dans
cette optique que s’inscrit le projet de conception logicielle présenté dans ce
document. \\


Pour les alumineries, nombreuses au Saguenay-Lac-Saint-Jean,
l’approvisionnement en énergie électrique est un enjeu majeur puisqu’il impacte
directement les coûts de production et la rentabilité des usines. C'est pourquoi la région compte six centrales hydroélectriques privées dont l'exploitation vise à maximiser la production d'énergie en tenant compte de nombreuses
contraintes difficilement maîtrisables. Des partenariats ont ainsi été établis avec des chercheurs de l’Université du Québec à Chicoutimi afin de produire des modèles mathématiques pouvant être soumis à différents processus
d’optimisation par ordinateur.  Les résultats générés par ces calculs constituent des outils d'aide à la décision primordiaux dans la planification des opérations des centrales hydroélectriques.  \\


\subsection{Production hydroélectrique, modélisation et optimisation}
\label{subsec:intro_produc}
Une centrale hydroélectrique est une usine qui récupère l’énergie cinétique de
l’eau pour entraîner des turbines. L’énergie mécanique ainsi produite est
ensuite convertie en énergie électrique grâce à des alternateurs \cite{hydroqc}.
Il existe des centrales au fil de l’eau, qui utilisent simplement le débit
naturel d’un cours d’eau afin de mouvoir les turbines, ainsi que des centrales
avec réservoir dont le principe de fonctionnement est illustré à la
figure \ref{fig:vue_coupe_centrale}. Ces dernières nécessitent des barrages,
ayant pour but de créer artificiellement une certaine hauteur de chute.  Des canalisations, appelées conduites forcées, relient le réservoir situé en amont du barrage au cours d'eau situé en aval.  Ces conduites sont dotées de turbines et sont conçues de manière à permettre l’écoulement de l’eau à haut débit dans le but de délivrer le maximum d'énergie cinétique possible afin d'entraîner les turbines.  Cette énergie cinétique est directement proportionnelle à la hauteur de chute \cite{uCalgary} \cite{kundur}. À la figure \ref{fig:vue_coupe_centrale}, la hauteur de chute brute correspond à la distance entre le bief
amont et le bief aval.  En pratique, la modélisation mathématique tient également compte des pertes en énergie, issues de la friction de l'eau sur les parois des conduites forcées, au moyen d'une correction apportée sur la hauteur de chute brute.  On obtient ainsi la hauteur de chute nette \cite{hChute}.\\


\begin{figure}[H]
    \centering
    \includegraphics[width=12cm]{vue_coupe_centrale}
    \caption{Vue en coupe d’une centrale hydroélectrique avec réservoir \cite{barrage}}
    \label{fig:vue_coupe_centrale} 
\end{figure}


La plupart des centrales hydroélectriques à haute puissance situées sur le territoire québécois sont dotées de réservoirs et
la gestion efficiente de la production d’électricité repose principalement sur la modulation efficace des volumes d'eau contenus par ces réservoirs. C’est pourquoi on a recours à des modèles mathématiques d’optimisation qui doivent tenir compte d'un très grand nombre de variables, dont certaines comportent intrinsèquement un haut degré d’incertitude. Par exemple, il est difficile de prévoir les quantités de précipitations qui affecteront localement un secteur particulier.  Il est donc d'autant plus ardu d'estimer adéquatement l'ensemble des apports hydriques naturels qui toucheront l'ensemble du bassin versant dont dépendent les niveaux des réservoirs d'un système de production hydroélectrique donné. \\

Différents types de modèles d’optimisation sont
employés par les producteurs, dont l’objectif est de déterminer à l’avance
comment opérer leur(s) centrale(s) en agissant sur les volumes des réservoirs,
les débits turbinés ainsi que les turbines à faire fonctionner pour une période
donnée afin de maximiser la production. Les fonctions de modélisation utilisées
peuvent différer grandement selon le cas (fonctions linéaires, non linéaires,
stochastiques, etc.) \cite{sSeguinLogiOptim}. \\


Afin de résoudre ces modèles, il existe des logiciels, communément appelés
solveurs d’optimisation, dont l’utilisation exige des connaissances approfondies
tant en mathématiques qu’en informatique. Il s’agit le plus souvent
d’applications en lignes de commandes dont les entrées sont les paramètres de la
fonction d’optimisation, qui doit donc être préalablement déterminée
manuellement. Il existe également des solutions commerciales intégrées telles
que MaxHydro 3 \cite{maxhydro} comportant des coûts importants ainsi qu’une
flexibilité limitée.


\subsection{Nature du projet}
\label{subsec:intro_nature}
Le but du projet présenté dans ce document est de développer un logiciel libre
(dit \textit{open-source}) comportant des interfaces graphiques simples et intuitives offrant des fonctionnalités
permettant de: 


\begin{itemize}
    \item charger un fichier d'entrée contenant les données brutes d'un modèle d'optimisation linéaire relatif à la production hydroélectrique
    \item réaliser l'extraction des paramètres du modèle d'optimisation (coefficients, bornes et contraintes)
    \item effectuer le traitement nécessaire afin de formatter les données afin de les rendre utilisables par une librairie d'optimisation
    \item résoudre le modèle d'optimisation
    \item proposer une représentation visuelle des solutions
\end{itemize}


Concrètement, le système devra permettre à l’usager de téléverser une fichier .csv ou .xls contenant l'ensemble des données nécessaires à la formulation d'un modèle d'optimisation linéaire. Par la suite, le système convertira les intrants dans un format adéquat pour le solveur approprié, appellera celui-ci et récupérera les résultats. Enfin, ces derniers pourront être sauvegardés sous forme de fichier et/ou affichés au moyen de graphiques appropriés, si l'utilisateur en fait la requête.  Le logiciel proposera donc une plateforme intégrée permettant de regrouper et de simplifier les processus de modélisation, d’optimisation et de mise en contexte des résultats.


\subsection{Caractérisation d’un modèle d’optimisation appliqué à la production hydroélectrique}
\label{subsec:def_modele_optimisation}
Afin d'introduire les notions mathématiques nécessaires à la compréhension du projet, la présente section
sera consacrée à l'analyse d'un exemple de modèle d’optimisation linéaire \cite{sSeguinPreciOptim}. On suppose un système de production hydroélectrique composé de deux centrales en série.  L'objectif formulé est la maximisation de l’énergie électrique produite. Chaque unité de production se compose d'un réservoir et d'une centrale. Le réservoir est représenté par un rectangle tandis qu'un triangle symbolise la centrale. La figure \ref{fig:centrales_series_modele} illustre cet exemple de système de production.


\begin{figure}[H]
    \centering
    \includegraphics[width=4cm]{centrale_serie}
    \caption{Schématisation de deux centrales avec réservoir placées en série.  Les coefficients \(q_i\) représentent les débits, tandis que les \(v_i\) désignent les volumes turbinés et \(a_i\) les apports naturels.}
    \label{fig:centrales_series_modele} 
\end{figure}


Conformément à l'objectif de maximisation de l'énergie produite par ce réseau, les processus de modélisation et d'optimisation visent à calculer deux types d'extrants, que l'on peut énoncer comme suit : pour plusieurs périodes \(t\) bien définies appartenant à un ensemble de périodes \(T\), les résultats seront les débits \(q\) et les volumes \(v\) à turbiner, pour chacune des unités de production \(c\) appartenant à l’ensemble \(C\) des unités composant le réseau de production.  Ces variables de sortie, dont les valeurs sont calculées par optimisation, s'appellent les variables de décision et constituent des prédictions servant à guider les décisions opérationnelles.  Ces variables de décision peuvent être décrites au moyen de la notation: \\


\begin{itemize}
    \item \(q_t^c\) : débit turbiné par la centrale \(c \in C\) à la période \(t
    \in T\)
    \item \(v_t^c\) : volume de la centrale \(c \in C\) à la période \(t \in T\)
    \\
\end{itemize}


Pour calculer les débits et les volumes optimaux pour chaque centrale et pour chaque période, il est nécessaire d'inclure dans le modèle tous les facteurs connus qui influencent la maximisation de la production d'énergie. L’expression mathématique de l'énergie est donnée par la multiplication de la puissance par une période de temps notée \(\Delta_t\). Le but de l'optimisation est décrit mathématiquement par la fonction objectif, et celle-ci prend la forme: \\

\[max\sum\limits_{t\in T}\sum\limits_{c\in C}P_t^c(\cdot)\Delta_t\] \\

\newpage

Les paramètres, c'est-à-dire les valeurs connues du problème, incluent les bornes et les contraintes auxquelles est soumise la fonction objectif.  Ils sont définis par: \\ 

\begin{itemize}
    \item \(a_t^c\) : apports naturels de la centrale \(c \in C\) à la période
    \(t \in T\)
    \item \(\underline{q_t^c}\) : débit minimal de la centrale \(c \in C\) à la
    période \(t \in T\)
    \item \(\overline{q_t^c}\) : débit maximal de la centrale \(c \in C\) à la
    période \(t \in T\)
    \item \(\underline{v_t^c}\) : volume maximal de la centrale \(c \in C\) à la
    période \(t \in T\) 
    \item \(\overline{v_t^c}\) : volume minimal de la centrale \(c \in C\) à la
    période \(t \in T\) 
    \item \(P_t^c(\cdot)\) : fonction de production de la centrale \(c \in C\) à la
    période \(t \in T\) 
    \item \(\zeta\) : facteur de conversion de \(m^3/s\) en \(hm^3\)
    \item \(\Delta_t\) : durée d'une période \(t\) \\
\end{itemize}


La fonction de production du système, ou fonction objectif, où \(P_t^c(\cdot)\) est multiplié par un intervalle temporel \(\Delta_t\), correspond à la puissance dans l’équation de l’énergie. Ayant identifié la fonction objectif, les bornes et les contraintes, le modèle d'optimisation complet peut être formalisé par son expression mathématique. Un modèle d’optimisation linéaire permettant de maximiser l'énergie produite dans un système de production hydroélectrique est donc donné par : \\


\begin{equation}
    max\sum\limits_{t\in T}\sum\limits_{c\in C}P_t^c(\cdot)\Delta_t \\ \label{eq:1}
\end{equation}

s.t. 

\begin{gather}
    v_{t+1}^c = v_t^c -q_t^c \zeta \Delta_t + a_t^c\zeta\Delta_t + \sum_{j = 1}^{J} q_t^j\zeta \Delta_t, \quad \forall c \in C, \forall j \in J, \forall t \in T \label{eq:2} \\[0.3cm]
    \underline{v_t^c} \leq v_t^c \leq \overline{v_t^c}, \quad \forall c \in C, \forall t \in T \label{eq:3} \\[0.3cm]
    \underline{q_t^c} \leq q_t^c \leq \overline{q_t^c}, \quad \forall c \in C, \forall t \in T \label{eq:4} \\[0.3cm]
    q_t^c \in \Re, \quad \forall t \in T \label{eq:5} \\[0.3cm]    
    v_t^c \in \Re, \quad \forall t \in T \label{eq:6} \\[0.3cm] \nonumber
\end{gather}


L’équation \ref{eq:1} représente la fonction objectif qui est sujette à (s.t.) une ou plusieurs contraintes.  L'équation \ref{eq:2} représente la loi de la conservation de l’eau, qui modélise la contribution des apports naturels en eau dans le système (pluie, ruissellement, etc.) et l’interdépendance entre le débit turbiné par une centrale en amont d’une autre et le volume du réservoir de cette dernière.  La borne \(J\) représente le nombre de centrales situées en amont de la centrale de référence.  Les équations \ref{eq:3} et \ref{eq:4} posent les bornes qui s'appliquent sur les variables de décision tandis que les équations \ref{eq:5} et \ref{eq:6} définissent leur domaine.  Afin de faciliter la compréhension des équations et, par le fait même, du modèle dans son ensemble, voici leurs descriptions explicites : \\


\begin{itemize}
    \item Eq. (\ref{eq:1}): L’objectif est de maximiser la somme de l’énergie générée par le réseau de production hydroélectrique \\
    \item Eq. (\ref{eq:2}): Pour l’ensemble des centrales et des périodes pour lesquelles une prédiction doit être calculée, le volume de la centrale c à la période \(t + 1\) doit correspondre au volume à la période \(t\) soustrait de la quantité d’eau débitée à la période \(t\) (Débit * temps * facteur de conversion \(m^3/s\) en \(hm^3\)) et additionné de la quantité d’eau fournie par les apports naturels et les volumes turbinés par la(les) centrale(s) en amont. \\

    \item Eq. (\ref{eq:3}): Pour l’ensemble des centrales et des périodes pour lesquelles une prédiction doit être calculée, le volume doit être compris dans l'intervalle défini par les volumes minimum et maximum imposés pour cette centrale. \\

    \item Eq. (\ref{eq:4}): Pour l’ensemble des centrales et des périodes pour lesquelles une prédiction doit être calculée, la quantité d’eau débitée à la période \(t\) doit être comprise dans l'intervalle défini par les débits minimum et maximum imposés pour cette centrale.
    \\
    \item Eq. (\ref{eq:5}): Pour l’ensemble des centrales et des périodes pour lesquelles une prédiction doit être calculée, le débit prédit appartient à l'ensemble des nombres réels. \\
    \item Eq. (\ref{eq:6}): Pour l’ensemble des centrales et des périodes pour lesquelles une prédiction doit être calculée, le volume prédit appartient à l'ensemble des nombres réels. \\
\end{itemize}

\begin{figure}[H]
    \centering
    \includegraphics[width=14cm]{vue_coupe_centrale_param}
    \caption{Illustration de la signification physique des paramètres du modèle d'optimisation dans le cas d'une centrale avec réservoirs en amont et en aval \cite{barrage}.}
    \label{fig:vue_coupe_centrale_param} 
\end{figure}

La figure \ref{fig:vue_coupe_centrale_param}, qui reprend la vue en coupe d'une centrale hydroélectrique, permet d'apprécier visuellement la signification des paramètres d'un modèle d'optimisation hydroélectrique concernant deux centrales avec réservoir placées en série, conformément à la représentation schématique de la figure \ref{fig:centrales_series_modele}. \\


En soumettant le modèle à un solveur d'optimisation, on obtient, pour l’ensemble des périodes et des centrales, les volumes et les débits à turbiner qui maximiseront l’énergie produite.  Cet exemple d’optimisation est de type "linéaire polynomial total", car la fonction objectif est un polynôme linéaire et le résultat fournit des décisions sur les débits et les volumes à turbiner pour chaque centrale. En contexte de production réel, une centrale possède habituellement plusieurs turbines. La somme des débits de chacune des turbines d’une centrale sera alors égale au débit total par centrale obtenu à l'aide du modèle polynomial total. Par contre, ce type d’optimisation, dit polynomial par turbine, ajoute un degré de complexité à la résolution en raison de l'introduction de variables binaires déterminant spécifiquement les turbines qui doivent être en fonction pour maximiser l'énergie, et ce pour chaque centrale et chaque période.  La portée du présent projet est toutefois limitée aux variables continues.

