\documentclass[../ingenierie_de_concepts.tex]{subfiles}

\section{Introduction et mise en contexte du projet} 
\label{sec:intro}
À l’ère de l’industrie 4.0, les entreprises qui sauront maximiser l’utilisation
des possibilités offertes par les nouvelles technologies bénéficieront
d’avantages concurrentiels majeurs essentiels à leur quête d’excellence.  La
collecte de quantités massives de données touchant pratiquement tous les aspects
des procédés industriels étant plus accessibles que jamais, le développement de
solutions visant à analyser et contextualiser ces données pour en extraire les
informations pertinentes et les représenter sous une forme intelligible pour les
décideurs de l’industrie revêt un degré d’importance sans précédent.  C’est dans
cette optique que s’inscrit le projet de conception logicielle présenté dans ce
document. \\

Chez Rio Tinto, qui opère cinq alumineries au Saguenay-Lac-Saint-Jean,
l’approvisionnement en énergie électrique est un enjeu majeur puisqu’il impacte
directement les coûts de production et la rentabilité des usines.  L’entreprise
est propriétaire de six centrales hydroélectriques dans la région et cherche
constamment à en maximiser le rendement en tenant compte de nombreuses
contraintes difficilement maîtrisables.  C’est pourquoi elle travaille en
collaboration avec des chercheurs de l’Université du Québec à Chicoutimi afin de
produire des modèles mathématiques pouvant être soumis à différents processus
d’optimisation par ordinateur. \\

\subsection{Production hydroélectrique, modélisation et optimisation}
\label{subsec:intro_produc}
Une centrale hydroélectrique est une usine qui récupère l’énergie cinétique de
l’eau pour entraîner des turbines. L’énergie mécanique ainsi produite est
ensuite convertie en énergie électrique grâce à des alternateurs \cite{hydroqc}.
Il existe des centrales au fil de l’eau, qui utilisent simplement le débit
naturel d’un cours d’eau afin de mouvoir les turbines, ainsi que des centrales
avec réservoir dont le principe de fonctionnement est illustré ci-bas à la
figure \ref{fig:vue_coupe_centrale}.  Ces dernières nécessitent des barrages,
ayant pour but de créer artificiellement une certaine hauteur de chute, ainsi
que des conduites forcées permettant l’écoulement de l’eau à haut débit pour
actionner les turbines avec une force décuplée, directement proportionnelle à la
hauteur de chute \cite{uCalgary} \cite{kundur}.  Sur la figure
\ref{fig:vue_coupe_centrale}, celle-ci correspond à la distance entre le bief
amont et le bief aval.   \\

\begin{figure}[H]
	\centering
	\includegraphics[width=12cm]{vue_coupe_centrale}
	\caption{Vue en coupe d’une centrale hydroélectrique avec réservoir \cite{barrage}}
	\label{fig:vue_coupe_centrale}   
\end{figure}

Les centrales opérées par Rio Tinto comportent majoritairement des réservoirs et
la gestion efficiente de la production d’électricité, reposant sur une
utilisation optimale de l’eau, s’avère complexe. C’est pourquoi on a recours à
des modèles mathématiques d’optimisation qui tiennent compte de nombreux
paramètres, dont certains comportent un haut degré d’incertitude.  Par exemple,
il est difficile de prévoir les quantités de précipitations qui affecteront les
niveaux des réservoirs.  Différents types de modèles d’optimisation sont
employés par les producteurs dont l’objectif est de déterminer à l’avance
comment opérer leur(s) centrale(s) en agissant sur les volumes des réservoirs,
les débits turbinés ainsi que les turbines à faire fonctionner pour une période
donnée afin de maximiser la production.  Les fonctions de modélisation utilisées
peuvent différer grandement selon le cas (fonctions linéaires, non linéaires,
stochastiques, etc.) \cite{sSeguinLogiOptim}. \\

Afin de résoudre ces modèles, il existe des logiciels, communément appelés
solveurs d’optimisation, dont l’utilisation exige des connaissances approfondies
tant en mathématiques qu’en informatique.   Il s’agit le plus souvent
d’applications en lignes de commandes dont les entrées sont les paramètres de la
fonction d’optimisation, qui doit donc être préalablement déterminée
manuellement.  Il existe également des solutions commerciales intégrées telles
que MaxHydro 3 \cite{maxhydro} comportant des coûts importants ainsi qu’une
flexibilité limitée.

\subsection{Nature du projet}
\label{subsec:intro_nature}
Le but du projet présenté dans ce document est de développer un logiciel libre
(“open-source”) comportant des interfaces graphiques simples et intuitives
permettant de réaliser: 

\begin{itemize}
	\item la schématisation de systèmes de production hydroélectrique (centrales
	et réservoirs)
	\item la saisie des paramètres (coefficients, contraintes, bornes, etc.) du
	modèle d’optimisation
	\item la génération automatique des fonctions d’optimisation correspondantes
	dans le format requis par le solveur utilisé
	\item la résolution du modèle par l’appel du solveur approprié
	\item la représentation explicite des solutions/résultats dans le format
	demandé par l’utilisateur
\end{itemize}

Concrètement, le système devra permettre à l’usager de modéliser un réseau de
production hydroélectrique en employant des icônes reliées entre elles à la
manière d’un diagramme-bloc et de saisir les contraintes et paramètres requis
selon le type de modèle.  Par la suite, le système convertira les intrants en
code utilisable par le solveur approprié, lancera celui-ci et récupérera les
résultats.  Enfin, divers moyens de visualisation (tableaux, graphiques, etc.)
seront proposés à l’usager afin d’offrir des représentations explicites des
résultats obtenus.  Le logiciel servira donc à regrouper et simplifier les
processus de modélisation, d’optimisation et de mise en contexte des résultats.

\subsection{Caractérisation d’un modèle d’optimisation appliqué à la production hydroélectrique}
\label{subsec:def_modele_optimisation}
Pour mieux mettre en contexte l’aspect mathématique de ce projet, cette section
sera dédiée à un exemple de modèle d’optimisation linéaire
\cite{sSeguinPreciOptim}. Supposons un système de production hydroélectrique
composé de deux centrales en série et supposons que l’objectif est de maximiser
l’énergie produite par ceux-ci. Chacun des systèmes possède un réservoir et une
centrale. Un rectangle symbolise le réservoir et un triangle symbolise la
centrale. Comme défini dans la section 1.2, la schématisation dans l’interface
devra permettre d’assembler un système similaire à la figure
\ref{fig:centrales_series_modele} ci-dessous.

\begin{figure}[H]
	\centering
	\includegraphics[width=4cm]{centrale_serie}
	\caption{Deux centrales en série}
	\label{fig:centrales_series_modele}   
\end{figure}

Pour atteindre l’objectif initial, le modèle d’optimisation nous fournira deux
informations : pour  plusieurs périodes \(t\) bien définies appartenant à un
ensemble \(T\) de périodes, les résultats seront les débits \(q\) et les volumes
\(v\) pour chacun des systèmes \(c\) appartenant à l’ensemble \(C\) des
centrales. Ces variables inconnues s'appellent les variables de décision et
peuvent être présentées sous la forme suivante : \\

\begin{itemize}
	\item \(q_t^c\) : débit turbiné par la centrale \(c \in C\) à la période \(t
	\in T\)
	\item \(v_t^c\) : volume de la centrale \(c \in C\) à la période \(t \in T\)
	\\
\end{itemize}

Ces débits et volumes correspondant à la solution du modèle d'optimisation, il
est important de savoir quels sont les facteurs (ou paramètres) qui sont
importants pour l’atteinte d’une énergie maximale. Premièrement, l’énergie est
la multiplication de la puissance par une période de temps. Le but ultime de la
démarche est décrit sous forme mathématique par la fonction objectif, et
celle-ci prend la forme suivante : \\

\[max\sum\limits_{c\in C}P^c(\cdot)\Delta\] \\

Ensuite, pour bien comprendre la fonction objectif, il faut aussi analyser
l’ensemble des paramètres impactant la solution : \\ 

\begin{itemize}
	\item \(a_t^c\) : apports naturels de la centrale \(c \in C\) à la période
	\(t \in T\)
	\item \(\underline{q_t^c}\) : débit minimal de la centrale \(c \in C\) à la
	période \(t \in T\)
	\item \(\overline{q_t^c}\) : débit maximal de la centrale \(c \in C\) à la
	période \(t \in T\)
	\item \(\underline{v_t^c}\) : volume maximal de la centrale \(c \in C\) à la
	période \(t \in T\) 
	\item \(\overline{v_t^c}\) : volume minimal de la centrale \(c \in C\) à la
	période \(t \in T\) 
	\item \(P^c(\cdot)\) : fonction de production de la centrale \(c \in C\) 
	\item \(\zeta\) : facteur de conversion de \(m^3/s\) en \(hm^3\)
	\item \(\Delta\) : durée d'une période \(t\) \\
\end{itemize}

La fonction de production de la centrale \(P^c(\cdot)\) correspond à la
puissance dans l’équation de l’énergie. Connaissant maintenant les variables de
décision et les paramètres et la fonction objectif, il faut définir les
contraintes et les limites du modèle d’optimisation. Un modèle d’optimisation
linéaire permettant de maximiser l'énergie produite dans un système de
production hydroélectrique est donné par : \\

\begin{equation}
	max\sum\limits_{c\in C}P^c(\cdot)\Delta 
\end{equation}

s.t. \\

\begin{align}
	v_{t+1}^c = v_t^c -q_t^c \zeta \Delta + a_t^c\zeta\Delta + \sum_{j = 1}^{J} q_t^j\zeta \Delta, \quad \forall c \in C, \forall j \in J, \forall t \in T \label{eq:2} \\[0.3cm]
	\underline{v_t^c} \leq v_t^c \leq \overline{v_t^c}, \hspace{1cm} \forall c \in C, \forall t \in T \label{eq:4} \\[0.3cm]
	\underline{q_t^c} \leq q_t^c \leq \overline{q_t^c}, \hspace{1cm} \forall c \in C, \forall t \in T \label{eq:4} \\[0.3cm]
	q_t^c \in \Re, \hspace{2.40cm} \forall t \in T \label{eq:4} \\[0.3cm]	 
	v_t^c \in \Re, \hspace{2.40cm} \forall t \in T \label{eq:4} \\[0.3cm] \nonumber
\end{align}

L’équation 1 représente la fonction objectif qui est sujette à (s.t.) une ou
plusieurs contraintes (équations 2 à 6).  Par exemple, l'équation 2 représente
la loi de la conservation de l’eau, qui introduit la contribution des apports
naturels en eau dans le système (pluie, ruissellement, etc.) et
l’interdépendance entre le débit turbiné par une centrale en amont d’une autre
et le volume du réservoir de cette dernière.  Pour faciliter la compréhension
des équations, voici leurs descriptions explicites : \\

\begin{enumerate}
	\item L’objectif est de maximiser la somme de l’énergie générée par les deux
	centrales. \\
	\item Pour l’ensemble des centrales et des périodes que l’on veut analyser,
	le volume de la centrale c à la période suivante doit correspondre au volume
	présent soustrait de la quantité d’eau débitée à la période présente (Débit
	* temps * facteur de conversion \(m^3/s\) en \(hm^3\)) et additionné de la
	quantité d’eau fournie par les apports naturels et les volumes turbinés par
	la(les) centrale(s) en amont. \\
	\item Pour l’ensemble des centrales et des périodes que l’on veut analyser,
	le volume présent doit être compris entre le volume minimum et maximum de
	cette centrale au moment présent. \\
	\item Pour l’ensemble des centrales et des périodes que l’on veut analyser,
	la quantité d’eau débitée présentement doit être comprise entre la quantité
	d’eau débitée minimum et maximum de cette centrale en fonction des périodes.
	\\
	\item Pour l’ensemble des centrales et des périodes que l’on veut analyser,
	la quantité d’eau débitée est un nombre réel. \\
	\item Pour l’ensemble des centrales et des périodes que l’on veut analyser,
	le volume est un nombre réel. \\
\end{enumerate}

En fournissant les paramètres, les contraintes, les bornes et la fonction
objectif à un solveur d'optimisation, on obtient, pour l’ensemble des périodes
et des centrales, les volumes et les débits qui maximisent l’énergie produite.
Cet exemple d’optimisation est de type "linéaire polynomial total", car la
fonction objectif est un polynôme linéaire et le résultat fournit les débits et
les volumes totaux par centrale. Dans un vrai contexte de système
hydroélectrique, une centrale possède plusieurs turbines. Simplement, la somme
des débits de chacune des turbines d’une centrale sera la même que la réponse en
type polynomial total. Par contre, ce type d’optimisation, dit polynomial par
turbine, ajoute un degré de complexité à la résolution, car des variables
binaires dicteront quelles turbines devront être en marche pour maximiser
l’énergie, et ce pour chaque centrale et chaque période.
